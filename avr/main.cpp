#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "avr/i2c.h"
#include "avr/uart.h"

#include <ttraveler/Application.h>

#include "Clock.h"
#include "DisplayPanel.h"
#include "linker.h"
#include "hardware/MCP23018.h"
#include "Timer.h"


using namespace ttraveler;


static Clock* globalClock = nullptr;
static MCP23018* globalDebugMCP = nullptr;
static Timer* globalTimer = nullptr;
static libtraveler::EventBuffer* globalEventBuffer = nullptr;


// Called when MCP23018_BAK detects a change in inputs
ISR(INT0_vect)
{
  uint8_t pressedButton;
  uint8_t pressedKey;

  globalDebugMCP->readInterruptedInputs(&pressedButton, &pressedKey);

  // find which button was pressed
  if (pressedButton & 0x01) {
    globalEventBuffer->pushButtonInput(libtraveler::Event::Button::Red);
  }
  if (pressedButton & 0x02) {
    globalEventBuffer->pushButtonInput(libtraveler::Event::Button::Green);
  }
  if (pressedButton & 0x04) {
    globalEventBuffer->pushButtonInput(libtraveler::Event::Button::Yellow);
  }
  if (pressedButton & 0x08) {
    globalEventBuffer->pushButtonInput(libtraveler::Event::Button::Control);
  }
  if (pressedButton & 0x10) {
    globalEventBuffer->pushButtonInput(libtraveler::Event::Button::Enter);
  }

  // find which key was pressed on keypad
  if (pressedKey != 0x0f) {
    globalEventBuffer->pushKeyboardInput(pressedKey);
  }
}

// Called when DS1307 updates its internal clock
ISR(INT1_vect)
{
  globalClock->newDateAvailable();
}


// Called when TIMER0 overflows
ISR(TIMER0_OVF_vect)
{
  globalTimer->registerTick();
}


int main()
{
  // disable all interrupts until the application is ready to run
  cli();

  // Configure clock interrupt
  PORTD |= _BV(PORTD2) | _BV(PORTD3);  // INT0 and INT1  as input with pull-up
  EICRA = _BV(ISC01) | _BV(ISC11);  // INT0 and INT1 interrupts of falling edge
  EIMSK = _BV(INT0) | _BV(INT1);  // enable INT0 and INT1 interrupt

  uart_init();
  i2c_init();

  _delay_ms(1000);  // give system some time to wake up

  // configure external dependencies
  DS1307 ds1307;
  ds1307.setDateTime({89, 10, 26, 9, 0, 0}, true);
  ds1307.configure(DS1307::CTRL_SQWE | DS1307::CTRL_SQ_1HZ);

  MCP23018 mcp23018{0x00};
  mcp23018.clearInterrupt();
  globalDebugMCP = &mcp23018;  // XXX: remove me

  // setup application
  libtraveler::DateHandler dateHandler{
    {1955, 10, 26, 1, 21},
    {1985, 10, 26, 1, 22},
    {1985, 10, 26, 1, 20}
  };
  libtraveler::EventBuffer eventBuffer;
  globalEventBuffer = &eventBuffer;

  Clock clock{
    dateHandler,
    eventBuffer,
    ds1307
  };
  globalClock = &clock;

  DisplayPanel displayPanel{mcp23018};

  Timer timer;
  globalTimer = &timer;

  libtraveler::Application app{
    clock,
      eventBuffer,
      dateHandler,
      displayPanel,
      timer,
  };

  sei();
  timer.startCounter();

  while (true) {
    clock.update();

    app.tick();
  }
}
