#pragma once

#include <ttraveler/Clock.h>

#include "hardware/DS1307.h"


namespace ttraveler
{

  class Clock:
    public libtraveler::Clock
  {
    public:
      Clock(libtraveler::DateHandler& dateHandler, libtraveler::EventBuffer& eventBuffer, DS1307& rtc);

      virtual void update() override;

      // Tells the clock that a new date is available
      void newDateAvailable();

    private:
      bool m_newDateAvailable = false;
      DS1307& m_rtc;
  };

}
