#include <avr/io.h>
#include <util/twi.h>

#include "i2c.h"

#define I2C_READY (TWCR & _BV(TWINT))
#define I2C_STATUS (TWSR & 0xf8)


namespace ttraveler {

  i2c_error_t latest_error;


  void i2c_init()
  {
    // frequency is around 50kHz here
    TWBR = 0x01;
    TWSR = 0x00;
  }


  bool check_status(uint8_t expected_status)
  {
    uint8_t retrieved_status = I2C_STATUS;

    if (retrieved_status != expected_status)
    {
      latest_error.expected_status = expected_status;
      latest_error.retrieved_status = retrieved_status;
      return false;
    }

    return true;
  }


  bool i2c_send_start(bool repeated)
  {
    TWCR = _BV(TWEN) | _BV(TWINT) | _BV(TWSTA);
    while (!I2C_READY);

    return check_status(!repeated ? TW_START : TW_REP_START);
  }


  void i2c_send_stop()
  {
    TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);
  }


  void i2c_send_byte(uint8_t byte)
  {
    TWDR = byte;
    TWCR = _BV(TWEN) | _BV(TWINT);

    while (!I2C_READY);
  }


  bool i2c_send_address(uint8_t address, bool read)
  {
    if (read)
    {
      address |= TW_READ;
    }

    i2c_send_byte(address);

    return check_status(!read ? TW_MT_SLA_ACK : TW_MR_SLA_ACK);
  }


  uint8_t i2c_read_byte(uint8_t address)
  {
    i2c_send_byte(address | TW_READ);

    uint8_t response = TWDR;

    TWCR = 0x84;
    return response;
  }


  bool i2c_write(uint8_t device_address, uint8_t register_address, uint8_t buffer[], uint8_t buffer_length)
  {
    // initiate and set internal register
    if (!i2c_send_start())
    {
      return false;
    }

    if (!i2c_send_address(device_address))
    {
      i2c_send_stop();
      return false;
    }

    // send address
    i2c_send_byte(register_address);
    if (!check_status(TW_MT_DATA_ACK))
    {
      i2c_send_stop();
      return false;
    }

    // send data
    for (uint8_t i = 0 ; i < buffer_length ; i++)
    {
      i2c_send_byte(buffer[i]);
      if (!check_status(TW_MT_DATA_ACK))
      {
        i2c_send_stop();
        return false;
      }
    }

    i2c_send_stop();

    return true;
  }


  bool i2c_read(uint8_t device_address, uint8_t register_address, uint8_t buffer[], uint8_t buffer_length)
  {
    if (!i2c_send_start())
    {
      return false;
    }

    // set the internal register pointer
    if (!i2c_send_address(device_address))
    {
      i2c_send_stop();
      return false;
    }

    i2c_send_byte(register_address);
    if (!check_status(TW_MT_DATA_ACK))
    {
      i2c_send_stop();
      return false;
    }

    // enter master receiver mode
    if (!i2c_send_start(true))
    {
      i2c_send_stop();
      return false;
    }

    if (!i2c_send_address(device_address, true))
    {
      i2c_send_stop();
      return false;
    }

    for (uint8_t i = 0 ; i < buffer_length ; i++)
    {
      if (i == buffer_length - 1)
      {
      TWCR = _BV(TWEN) | _BV(TWINT);
      }
      else
      {
      TWCR = _BV(TWEN) | _BV(TWINT) | _BV(TWEA);
      }

      while (!I2C_READY);
      buffer[i] = TWDR;
    }

    i2c_send_stop();

    return true;
  }

  const i2c_error_t* i2c_error()
  {
    return &latest_error;
  }

}
