/**
 * Blocking I2C utility for master mode.
 */

#pragma once

#include <stdint.h>


namespace ttraveler
{

  struct i2c_error_t {
    uint8_t expected_status;
    uint8_t retrieved_status;
  };


  /**
   * Sets a fixed frequency.
   * Call this before using any other related I2C functions
   */
  void i2c_init();

  /**
   * Send a start condition.
   *
   * @return a boolean indicating if the operation was successfull.
   */
  bool i2c_send_start(bool repeated=false);

  /**
   * Sends a stop condition.
   */
  void i2c_send_stop();

  /*
   * Sends a single byte
   *
   * @param byte the byte to send
   *
   * @return wheter or not the ACK bit was set
   */
  void i2c_send_byte(uint8_t byte);

  /*
   * @param address the device address
   * @param param set to true if the read bit should be set. false if the write
   *    bit should be set
   *
   * @return whether or not the ACK bit was set by the slave
   */
  bool i2c_send_address(uint8_t address, bool read=false);

  uint8_t i2c_read_byte(uint8_t address);


  bool i2c_write(uint8_t device_address, uint8_t register_address, uint8_t buffer[], uint8_t buffer_length);

  /*
   * Read some bytes from an address.
   * I2C transmission should not already been started.
   *
   * This assume a first write to the device with the register address to read, then
   * an arbitrary number of reads.
   */
  bool i2c_read(uint8_t device_address, uint8_t register_address, uint8_t buffer[], uint8_t buffer_length);

  /**
   * Retrieve latest I2C error
   */
  const i2c_error_t* i2c_error();

}
