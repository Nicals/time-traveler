#define BAUD 9600

#include <avr/io.h>
#include <util/setbaud.h>
#include <stdio.h>

#include "uart.h"


namespace ttraveler
{

  void uart_init()
  {
    UBRR0 = UBRR_VALUE;
#if USE_2X
    UCSR0A |= _BV(U2X0);
#else
    UCSR0A &= _BV(U2X0);
#endif

    // set word size to 8 bits
    UCSR0C |= _BV(UCSZ01) | _BV(UCSZ00);
    // enable transmission
    UCSR0B |= _BV(TXEN0);
  }


  void uart_write_byte(uint8_t byte)
  {
    while ((UCSR0A & _BV(UDRE0)) == 0);
    UDR0 = byte;
  }


  void uart_write_string(const char* bytes)
  {
    while (*bytes != 0x00)
    {
      uart_write_byte(*bytes);
      bytes++;
    }
    uart_write_byte('\n');
  }


  void uart_write_hex(uint8_t byte) {
    char str[5];
    snprintf(str, 5, "0x%02x", byte);
    uart_write_string(str);
  }

}
