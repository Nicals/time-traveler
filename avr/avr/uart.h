/*
 * Quick and dirty UART utilities, mainly used for debug purpose
 *
 * The protocol is set as following:
 *   + 9600 bauds
 *   + 8 bit word size
 *   + no parity bit
 *   + 1 stop bit
 */
#pragma once

#include <stdint.h>


namespace ttraveler {

  /**
   * Init uart for transmission only
   */
  void uart_init();

  /**
   * Sends a byte on uart
   */
  void uart_write_byte(uint8_t byte);

  void uart_write_string(const char* bytes);

  void uart_write_hex(uint8_t byte);

}
