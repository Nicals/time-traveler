#pragma once

#include <ttraveler/DisplayPanel.h>


using BaseDisplayPanel = libtraveler::DisplayPanel;
using libtraveler::Date;
using libtraveler::SettingsValue;


namespace ttraveler
{

  class MCP23018;

  class DisplayPanel:
    public BaseDisplayPanel
  {
    public:
      DisplayPanel(MCP23018& controlPanel);
      virtual ~DisplayPanel() {};

      void clearRow1() override final;
      void clearRow2() override final;
      void clearRow3() override final;

      void updateRow1(const Date& date) override final;
      void updateRow2(const Date& date) override final;
      void updateRow3(const Date& date) override final;

      void updateRow1(const SettingsValue& value) override final;
      void updateRow2(const SettingsValue& value) override final;
      void updateRow3(const SettingsValue& value) override final;

      void updateButtonState(
          BaseDisplayPanel::ButtonState buttons,
          ButtonState mask = BaseDisplayPanel::AllButtons
      ) override final;

    private:
      MCP23018& m_controlPanel;
  };

}
