// Provides functions needed for the linker

#pragma once


// For some reasons, this is needed for polymorphism on AVR.
// Basically, provides a function for pure virtual method if they ever
// get called.
//
// I've seen some people implementing it as an infinite loop to trap the
// program in case of bug.
// Still not sure how I would handle a pure virtual function call if it ever
// happens so lets do nothing and face the consequences.
//
// see: https://stackoverflow.com/questions/920500/what-is-the-purpose-of-cxa-pure-virtual
//      https://www.avrfreaks.net/forum/avr-c-micro-how
extern "C" void __cxa_pure_virtual() { }

extern "C" void operator delete(void* v, unsigned int i) { }
