#pragma once

#include <stdint.h>


namespace ttraveler
{

  class MCP23018
  {
    public:
      /**
       * Setup the device
       *
       * Arguments:
       *    address: The three lsb bits of the address
       */
      MCP23018(uint8_t address);

      /**
       * Clear any interrupt currently raised
       */
      void clearInterrupt() const;

      /**
       * Extracts pressed button and pressed key from current interrupt state.
       * Calling this method will NOT clear any interrupt state.
       *
       * Any pressed button will be writtin in pressedButton as 1 bit, lsb bit
       * being Red button, then Green, Yellow, Control and Enter.
       *
       * The pressed key is written in pressed key as the actual value (0-9)
       * pressed. If no key was pressed, the value 0x0f should be written.
       * No other value is expected, but who knows...
       */
      void readInterruptedInputs(uint8_t* pressedButton, uint8_t* pressedKey) const;

      /**
       * Switch on or of the button LEDs (1 being obviously on).
       * Only the leds sets in mask will be changed, all other pin values
       * will be kept.
       */
      void setButtonLed(uint8_t leds, uint8_t mask) const;

    private:
      // Device base address
      static constexpr uint8_t ADDRESS = 0x40;

      // Register address
      static constexpr uint8_t IODIR_REGISTER = 0x00;
      static constexpr uint8_t IOCON_REGISTER = 0x0a;
      static constexpr uint8_t INTCAP_REGISTER = 0x10;
      static constexpr uint8_t GPIO_REGISTER = 0x12;
      static constexpr uint8_t INTF_REGISTER = 0x0e;

      // IOCON settings
      static constexpr uint8_t IOCON_INTCC = 1;
      static constexpr uint8_t IOCON_MIRROR = 1 << 6;

      // Port mask
      static constexpr uint8_t BTN_LED_MASK = 0x1f;
      static constexpr uint8_t BTN_INPUT_MASK = 0x1f;
      static constexpr uint8_t PORTA_KBD_MASK = 0x20;
      static constexpr uint8_t PORTB_KBD_MASK = 0xe0;

      uint8_t m_address;
  };

}
