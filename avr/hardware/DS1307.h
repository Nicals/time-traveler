#pragma once

#include <stdint.h>


namespace ttraveler
{

  class DS1307
  {
    public:
      struct DateTime {
        uint8_t year;
        uint8_t month;
        uint8_t day;
        uint8_t hours;
        uint8_t minutes;
        uint8_t seconds;
      };

      void configure(uint8_t ctrl) const;

      /**
       * Set current time. 24 hour mode is assumed.
       * Year is in range 0-99
       */
      void setDateTime(const DS1307::DateTime& datetime, bool enableOscillator = true) const;
      DS1307::DateTime getDateTime() const;

      void writeInRam(uint8_t address, uint8_t data[], uint8_t length) const;
      void readFromRam(uint8_t address, uint8_t buffer[], uint8_t length) const;

      // Control register values. See datasheets
      static constexpr uint8_t CTRL_OUT = 1 << 7;
      static constexpr uint8_t CTRL_SQWE = 1 << 4;
      static constexpr uint8_t CTRL_SQ_1HZ = 0x00;
      static constexpr uint8_t CTRL_SQ_4kHZ = 0x01;
      static constexpr uint8_t CTRL_SQ_8kHZ = 0x2;
      static constexpr uint8_t CTRL_SQ_32kHZ = 0x03;

    private:
      uint8_t intToBcd(uint8_t value) const;
      uint8_t bcdToInt(uint8_t value) const;

      static constexpr uint8_t ADDRESS = 0xd0;

      static constexpr uint8_t SECONDS_REGISTER = 0x00;
      static constexpr uint8_t CONTROL_REGISTER = 0x07;
      static constexpr uint8_t RAM_REGISTER = 0x08;

      static constexpr uint8_t CLOCK_RUN = 0x00;
      static constexpr uint8_t CLOCK_HALT = 1 << 7;
  };

}
