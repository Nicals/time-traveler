#include "MCP23018.h"

#include "../avr/i2c.h"


namespace ttraveler
{

  MCP23018::MCP23018(uint8_t address)
    : m_address{ADDRESS | (address & 0x07)}
  {
    constexpr uint8_t BUFFER_SIZE = 14;

    const uint8_t iocon_settings = IOCON_MIRROR | IOCON_INTCC;

    uint8_t buffer[BUFFER_SIZE] = {
      // Direction (IODIR)
      // When a bit is set, the pin is an input
      //   Ports A0-4 are button LED output (LED turned-on on low output)
      //   Ports B0-4 are button input
      //   Ports B5-7 are keypad input bits 0-2
      //   Port A5 is keypad input bit 3
      PORTA_KBD_MASK,
      PORTB_KBD_MASK | BTN_INPUT_MASK,
      // Polarity (IPOL)
      //   Button inputs are in pull-up configuration, we invert their values
      PORTA_KBD_MASK,
      PORTB_KBD_MASK | BTN_INPUT_MASK,
      // Enable interrupts for input (GPINTEN)
      PORTA_KBD_MASK,
      PORTB_KBD_MASK | BTN_INPUT_MASK,
      // Dummy value, unused (DEFVAL)
      0x00, 0x00,
      // Interrupt triggered by comparing with previous value (INTCON)
      0x00, 0x00,
      // Device settings (IOCON)
      //   Interrupt lines are mirrored
      //   Interrupt is active low
      //   Interrupt clearing on GPIO read
      iocon_settings,
      iocon_settings,
      // Pull-up on input ports (GPPU)
      PORTA_KBD_MASK,
      PORTB_KBD_MASK | BTN_INPUT_MASK
    };

    i2c_write(m_address, IODIR_REGISTER, buffer, BUFFER_SIZE);
  }

  void MCP23018::clearInterrupt() const
  {
    uint8_t buffer[2];

    i2c_read(m_address, INTCAP_REGISTER, buffer, 2);
  }

  void MCP23018::readInterruptedInputs(uint8_t* pressedButton, uint8_t* pressedKey) const
  {
    // Will contains INFA, INTFB, INTCAPA and INTCAPB
    uint8_t buffer[4];

    i2c_read(m_address, INTF_REGISTER, buffer, 4);

    *pressedButton = buffer[1] & buffer[3];
    *pressedKey = ((PORTA_KBD_MASK & buffer[2]) >> 2) | (buffer[3] >> 5);
  }

  void MCP23018::setButtonLed(uint8_t leds, uint8_t mask) const
  {
    uint8_t portA;
    i2c_read(m_address, GPIO_REGISTER, &portA, 1);

    // extract led state, logical 1 being switched on light
    uint8_t ledCmd = ~portA & BTN_LED_MASK;
    // clear masked LED
    ledCmd &= ~mask;
    // set button state
    ledCmd |= leds;

    // clear port1 led values
    portA &= ~BTN_LED_MASK;
    // inject led cmd
    portA |= ~ledCmd;


    i2c_write(m_address, GPIO_REGISTER, &portA, 1);
  }
}
