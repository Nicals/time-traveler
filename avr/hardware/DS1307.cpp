#include "DS1307.h"

#include "../avr/i2c.h"
// XXX: REMOVE ME
#include "../avr/uart.h"


namespace ttraveler
{
  void DS1307::setDateTime(const DS1307::DateTime& datetime, bool enableOscillator) const
  {
    uint8_t cmd[7] = {
      (enableOscillator ? DS1307::CLOCK_RUN : DS1307::CLOCK_HALT) | intToBcd(datetime.seconds),
      intToBcd(datetime.minutes),
      intToBcd(datetime.hours) & 0x3f,
      0x00,
      intToBcd(datetime.day),
      intToBcd(datetime.month),
      intToBcd(datetime.year),
    };

    if (!i2c_write(DS1307::ADDRESS, DS1307::SECONDS_REGISTER, cmd, 7)) {
      uart_write_string("failed to write time");
    }
  }

  DS1307::DateTime DS1307::getDateTime() const
  {
    uint8_t buffer[7];

    if (!i2c_read(DS1307::ADDRESS, DS1307::SECONDS_REGISTER, buffer, 7)) {
      uart_write_string("failed to read dt");
    }

    return {
      bcdToInt(buffer[6]),
      bcdToInt(buffer[5]),
      bcdToInt(buffer[4]),
      bcdToInt(buffer[2] & 0x3f),
      bcdToInt(buffer[1]),
      bcdToInt(0x7f & buffer[0]),
    };
  }

  void DS1307::configure(uint8_t ctrl) const
  {
    if (!i2c_write(DS1307::ADDRESS, DS1307::CONTROL_REGISTER, &ctrl, 1)) {
      uart_write_string("failed to configure");
    }
  }

  void DS1307::writeInRam(uint8_t address, uint8_t data[], uint8_t length) const
  {
    if (!i2c_write(DS1307::ADDRESS, DS1307::RAM_REGISTER + address, data, length)) {
      uart_write_string("failed to write to RAM");
    }
  }

  void DS1307::readFromRam(uint8_t address, uint8_t buffer[], uint8_t length) const
  {
    if (!i2c_read(DS1307::ADDRESS, DS1307::RAM_REGISTER + address, buffer, length)) {
      uart_write_string("failed to write from RAM");
    }
  }

  uint8_t DS1307::intToBcd(uint8_t value) const
  {
    return ((value / 10) << 4) + (value % 10);
  }

  uint8_t DS1307::bcdToInt(uint8_t value) const
  {
    return ((value & 0xf0) >> 4) * 10 + (value & 0x0f);
  }

}
