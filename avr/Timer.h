#pragma once

#include <stdint.h>

#include <ttraveler/Timer.h>


namespace ttraveler
{

  class Timer: public libtraveler::Timer
  {
    public:
      Timer();

      void startCounter();
      void registerTick();

    protected:
      void onStart() override;
      bool hasTimedOut() override;

    private:
      static constexpr uint8_t STOP_CYCLES = 5;

      bool m_timedOut = false;
      uint8_t m_cycle = 0;
  };

}
