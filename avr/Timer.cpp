#include "Timer.h"

#include <avr/io.h>


namespace ttraveler
{

  Timer::Timer()
  {
    // TIMER0 in normal mode
    TCCR0A = 0x00;
    // Interrupt mask on TIMER0 counter overflow
    TIMSK0 = 0x01;
    // Disable the clock
    TCCR0B = 0x00;
  }

  void Timer::startCounter()
  {
    // TIMER0 clock with 1024 prescaler
    TCCR0B = 0x05;
  }

  void Timer::registerTick()
  {
    if (!m_started) {
      return;
    }

    if (m_cycle == 0) {
      m_timedOut = true;
    }
    else {
      m_cycle--;
    }
  }

  void Timer::onStart()
  {
    m_cycle = STOP_CYCLES;
  }

  bool Timer::hasTimedOut()
  {
    if (!m_timedOut) {
      return false;
    }

    m_timedOut = false;

    return true;
  }

}
