#include "Clock.h"


namespace ttraveler
{

  Clock::Clock(libtraveler::DateHandler& dateHandler, libtraveler::EventBuffer& eventBuffer, DS1307& rtc)
    : libtraveler::Clock{dateHandler, eventBuffer},
      m_rtc{rtc}
  {
  }

  void Clock::newDateAvailable()
  {
    m_newDateAvailable = true;
  }

  void Clock::update()
  {
    if (!m_newDateAvailable) {
      return;
    }

    DS1307::DateTime chipDate = m_rtc.getDateTime();
    setDate({
      2000 + chipDate.year,
      chipDate.month,
      chipDate.day,
      chipDate.hours,
      chipDate.minutes,
      chipDate.seconds,
    });

    m_newDateAvailable = false;
  }

}
