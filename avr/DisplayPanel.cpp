#include "DisplayPanel.h"

#include "hardware/MCP23018.h"
// XXX: debug method waiting for proper front panel implementation
#include "avr/uart.h"


namespace ttraveler
{

  DisplayPanel::DisplayPanel(MCP23018& controlPanel)
    : m_controlPanel{controlPanel}
  {
  }

  void DisplayPanel::clearRow1()
  {
    uart_write_string("clear 1");
  }

  void DisplayPanel::clearRow2()
  {
    uart_write_string("clear 2");
  }

  void DisplayPanel::clearRow3()
  {
    uart_write_string("clear 2");
  }

  void DisplayPanel::updateRow1(const Date& date)
  {
    uart_write_string("date 1");
  }

  void DisplayPanel::updateRow2(const Date& date)
  {
    uart_write_string("date 2");
  }

  void DisplayPanel::updateRow3(const Date& date)
  {
    uart_write_string("date 3");
  }

  void DisplayPanel::updateRow1(const SettingsValue& value)
  {
    uart_write_string("settings 1");
  }

  void DisplayPanel::updateRow2(const SettingsValue& value)
  {
    uart_write_string("settings 1");
  }

  void DisplayPanel::updateRow3(const SettingsValue& value)
  {
    uart_write_string("settings 3");
  }

  void DisplayPanel::updateButtonState(ButtonState buttons, ButtonState mask)
  {
    m_controlPanel.setButtonLed(buttons, mask);
  }

}
