Time Traveler
=============

This project is used to travel in time.
It is an implementation of the Zemeckis-Gale model.

Time Traveler is organized in three parts.
The base library (*lib/* directory) implements the main engine.
The tests executable (*tests/* directory) test the engine on a classic computer.
The avr executable (*avr/* directory) implements the engine on an AVR uc.
The curses executable (*curses/* directory) implements the engine for console display.


Build
-----

Dependencies:

  + ncurses (for curses build)

Classic build here:

```shell-session
$ mkdir build
$ cd build
$ cmake ..
$ make
```

Curses build can be activated with `-DWITH_CURSES=ON`.
Unit tests build can bit activated with `-DWITH_TESTS=ON`.


### AVR build

Dependencies:

  + avr-libc
  + avrdude

To build the project for avr:

```shell-session
$ mkdir build-avr
$ cd build-avr
$ cmake .. -DAVR
```

Options:

  + `MCU` (default to *atmega328p*)
  + `PROGRAMMER` (default to *usbtiny*)

Targets:

  + `flash` send compiled program to mcu
