Operations
==========

This file explains the time traveler operations procedures.

UI description
--------------

UI elements are as follow:

  + A 10 digits numeric keyboard
  + A row of 5 push buttons with backlights leds (red, green, yellow and two white) 
  + 3 rows of text display

The keyboard is designated **KB**.

The red push button is **RP**, its led is **RL**.
Green and yellow follow the same convention: **GP**, **GL**, **YP** and **YL**.

One of the white push button is used to validate commands (enter).
The button and led are named **EP** and **EL**.

The last white push button is used to display the control menu.
We name its component **CP** and **CL**.

Button order is as follow:

  + Red, Green, Yellow, Control, Enter

The three rows of text are designated **T1**, **T2** and **T3**.

### Text rows

Text rows contains the following elements:

  + month: 3 letters
  + day: 2 digits
  + year: 4 digits
  + AM/PM: 2 leds, one for each element (**APL**)
  + hour: 2 digits
  + half clock: two leds (colon sign) that lights on and off every half a sec (**HCL**)
  + minutes: 2 digits


Error mode
----------

Whenever an input error occures, the error mode is triggered.
Error mode will lock all input for a given time (let it be 1 sec) and turn off the **RL** light.

On top of that, as soon as the error is detected, an error sound will be played.

Once the lock time has passed, the input is unlocked and **RL** switches off and we switch back to
the previous mode that triggered the error.

Error mode will only lock the user inputs and change the state of **RL**.
Any other display elements will remain as they are.


Default mode
------------

In default mode, **T1-3** display dates.
**T1** shows the destination date (when we want to go), **T2** the current date and **T3** is a
buffer for the last date set.

By default, the following date are shown:

  + DEST: OCT 05 1955 AM 01 21
  + PRES: now()
  + LAST: OCT 26 1985 AM 01 20

All push button light are off and the system is ready to accept commands.

When a commmand is successfuly entered **CL** will flash.
When a command is being entered, **GL** is on.
When a command being entered is invalid, the internal command buffer is cleared, **GL**
turns off and **RL** turns on.

**RL** is used to show errors and won't switch off until either **RP** is pressed or a new command
is being entered.
TODO: For now, we lock and light **RL** for 1 sec... Don't really know which one is best

Each seconds, the display is updated, a sound is played and **HCL** goes on for half a second.


### Set current date

Enter date digits on **KB**, then press enter (**GP**).
If the date is valid, **CL** will flash and **T2** is updated.

During input, if at any time one of the input will prevent the creation of a valid date, the error
mode will be triggered.
On error, the internal buffer is clear and user has to input the date from the beginning.


### Set destination date

Enter date digits on **KB**, then press **EP**.
If the date is valid, **CL** will flash **T1** is updated with the new date.

As with *Set current date*, any error in the input will trigger the error mode and clear the
internal buffer.


### Exchange dest/last dates

A press on **YP** will exchanges the values in **T1** and **T3**.
The **CL** will flash.


### Time Travel

Whenever the current date (**T2**) reaches the value of the destination date (**T1**), time travel
is initiaded: the current date is in fact the same as the destination one (yes, it actually works).

Optionally, time travel can also be triggered by connecting a flux capacitor to the device and reaching a
velocity of at least 1.339e-05% of the speed of light.

Whenever time travel is achieve, an alarm sound is triggered and **RL** turns on.
Alarm sound and **RL** are cleared by pressing **RP**.


### Switch to menu mode

Pressing **CP** will switch to menu mode.
Any internal input buffer is cleared.


Menu mode
---------

This mode allows to modify the internal settings of the time travel machine.
Each of the display rows show an settings that can be changed by the user.
The selected settings is alway the one in **T1**.

**APL** and **HCL** will be all on on **T1** to show that this row is the selected one.
Only the *month* and *year* display on each row will have a value.

The *month* 3 letters display is used for settings label.
The *year* 4 digits display is used to show the current setting.

Settings selection can be change with **RP** (up) and **YP** (down).
This will scroll the display up and down (current settings always being the top one).

Settings values can be either *enum* or *input*.
On **enum** settings, **GP** presses will cycle through predefined values.
On **input** settings, **KB** strokes are input, then entered by **GP** press.
Any error on **input** settings will trigger the error mode and reset the current input buffer.

Pressing **CP** will go back to *default mode*.

| Name       | Displayed name | Type  | Range | Description                                          |
| ----       | -------------- | ----  | ----- | -----------                                          |
| Sound      | SND            | enum  | 0-1   | Switches of the sound output                         |
| Brightness | BRT            | input | 0-100 | Set **T1-3** brightness (0 being lowest but visible) |
