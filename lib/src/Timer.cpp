#include <ttraveler/Timer.h>


namespace libtraveler
{

  void Timer::start()
  {
    if (m_started) {
      return;
    }

    m_started = true;
    onStart();
  }

  void Timer::checkTimeout()
  {
    if (!m_started) {
      return;
    }

    if (!hasTimedOut()) {
      return;
    }

    m_started = false;

    if (m_observer != nullptr) {
      m_observer->onTimeout();
    }
  }

  void Timer::setObserver(TimerObserver* observer)
  {
    m_observer = observer;
  }

}
