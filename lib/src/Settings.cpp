#include <ttraveler/Settings.h>


namespace libtraveler
{

  SettingsValue::SettingsValue(const char* name, int initial, int maxValue)
    : m_value{initial},
      m_maxValue{maxValue}
  {
    for (int i = 0 ; i < 3 ; i++) {
      m_name[i] = name[i];
    }
  }

  const char* SettingsValue::name() const
  {
    return m_name;
  }

  int SettingsValue::value() const
  {
    return m_value;
  }

  void SettingsValue::setValue(int value)
  {
    m_value = value;
  }

  int SettingsValue::maxValue() const
  {
    return m_maxValue;
  }


  Settings::Settings()
    : m_soundEnabled{"SND", 1},
      m_brightness{"BRT"}
  {
  }

  SettingsValue& Settings::soundEnabled()
  {
    return m_soundEnabled;
  }

  SettingsValue& Settings::brightness()
  {
    return m_brightness;
  }

}
