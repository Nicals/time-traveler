#include <ttraveler/modes/TimeTravelMode.h>


namespace libtraveler
{

  TimeTravelMode::TimeTravelMode(DateHandler& dateHandler)
    : m_dateHandler{dateHandler}
  {
  }

  void TimeTravelMode::onEnter(DisplayPanel& displayPanel)
  {
    displayPanel.updateRow1(m_dateHandler.dest());
    displayPanel.updateRow2(m_dateHandler.current());
    displayPanel.updateRow3(m_dateHandler.previous());
  }

  Mode::InputResult TimeTravelMode::onInput(Event input, DisplayPanel& displayPanel)
  {
    if (input.type == Event::ButtonPressed) {
      if (input.button == Event::Button::Enter) {
        return setDestDate(displayPanel);
      }

      if (input.button == Event::Button::Green) {
        return setCurrentDate(displayPanel);
      }

      if (input.button == Event::Button::Yellow) {
        return swapDestDate(displayPanel);
      }
    }
    else if (input.type == Event::KeyboardPressed) {
      return injectDateInput(input.key);
    }
    else if (input.type == Event::TimeWentOn) {
      displayPanel.updateRow2(m_dateHandler.current());
    }

    return Mode::None;
  }

  Mode::InputResult TimeTravelMode::swapDestDate(DisplayPanel& displayPanel)
  {
    m_dateHandler.swapDest();

    displayPanel.updateRow1(m_dateHandler.dest());
    displayPanel.updateRow3(m_dateHandler.previous());

    return Mode::Success;
  }

  Mode::InputResult TimeTravelMode::injectDateInput(Event::Key key)
  {
    if (not m_dateBuilder.push(key)) {
      m_dateBuilder.clear();
      return Mode::Failure;
    }

    return Mode::Ok;
  }

  Mode::InputResult TimeTravelMode::setDestDate(DisplayPanel& displayPanel)
  {
    if (not m_dateBuilder.hasContent()) {
      m_dateBuilder.clear();
      return Mode::Failure;
    }

    m_dateHandler.setDest(m_dateBuilder.getDate(m_dateHandler.dest()));
    m_dateBuilder.clear();

    displayPanel.updateRow1(m_dateHandler.dest());

    return Mode::Success;
  }

  Mode::InputResult TimeTravelMode::setCurrentDate(DisplayPanel& displayPanel)
  {
    if (not m_dateBuilder.hasContent()) {
      m_dateBuilder.clear();
      return Mode::Failure;
    }

    m_dateHandler.setCurrent(m_dateBuilder.getDate(m_dateHandler.current()));
    m_dateBuilder.clear();

    displayPanel.updateRow2(m_dateHandler.current());

    return Mode::Success;
  }

}
