#include <ttraveler/modes/SettingsHandler.h>

#include <ttraveler/Settings.h>


namespace libtraveler
{

  SettingsValueHandler::SettingsValueHandler(SettingsValue& settingsValue)
    : m_settingsValue{settingsValue}
  {
  }

  const SettingsValue& SettingsValueHandler::settingsValue() const
  {
    return m_settingsValue;
  }


  BoolValueHandler::BoolValueHandler(SettingsValue& settingsValue)
    : SettingsValueHandler{settingsValue}
  {
  }

  Mode::InputResult BoolValueHandler::consumeInput(Event input)
  {
    if (input.type == Event::ButtonPressed and input.button == Event::Button::Green) {
      m_settingsValue.setValue(m_settingsValue.value() ? 0 : 1);

      return Mode::InputResult::Success;
    }
    return Mode::InputResult::None;
  }


  IntValueHandler::IntValueHandler(SettingsValue& settingsValue)
    : SettingsValueHandler{settingsValue}
  {
  }

  void IntValueHandler::clearState()
  {
    m_buffer = 0;
  }

  Mode::InputResult IntValueHandler::consumeInput(Event input)
  {
    if (input.type == Event::KeyboardPressed) {
      m_buffer = m_buffer * 10 + input.key;

      if (m_buffer > m_settingsValue.maxValue()) {
        clearState();

        return Mode::Failure;
      }
    }
    else if (input.type == Event::ButtonPressed and input.button == Event::Button::Green) {
      m_settingsValue.setValue(m_buffer);
      clearState();

      return Mode::InputResult::Success;
    }

    return Mode::InputResult::None;
  }
}
