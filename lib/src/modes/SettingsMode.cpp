#include <ttraveler/modes/SettingsMode.h>

#include <ttraveler/Settings.h>


namespace libtraveler
{

  SettingsMode::SettingsMode(Settings& settings)
    : m_settings{settings},
      m_soundEnabledHandler{settings.soundEnabled()},
      m_brightnessHandler{settings.brightness()}
  {
    m_settingsHandlers[0] = &m_soundEnabledHandler;
    m_settingsHandlers[1] = &m_brightnessHandler;
  }

  void SettingsMode::onEnter(DisplayPanel& displayPanel)
  {
    refreshDisplay(displayPanel);
  }

  Mode::InputResult SettingsMode::onInput(Event input, DisplayPanel& displayPanel)
  {
    if (input.type == Event::ButtonPressed) {
      if (input.button == Event::Button::Yellow) {
        if (m_currentHandlerIndex > 0) {
          m_currentHandlerIndex--;
          refreshDisplay(displayPanel);
        }

        return Mode::None;
      }
      else if (input.button == Event::Button::Red) {
        if (m_currentHandlerIndex + 1 < m_handlersCount) {
          m_currentHandlerIndex++;
          refreshDisplay(displayPanel);
        }

        return Mode::None;
      }
    }

    auto currentMode = m_settingsHandlers[m_currentHandlerIndex];
    auto handlerResult = currentMode->consumeInput(input);

    if (handlerResult == Mode::Success) {
      displayPanel.updateRow2(currentMode->settingsValue());
    }

    return handlerResult;
  }

  void SettingsMode::refreshDisplay(DisplayPanel& displayPanel) const
  {
    int rowIdx1 = m_currentHandlerIndex - 1;
    int rowIdx2 = m_currentHandlerIndex;
    int rowIdx3 = m_currentHandlerIndex + 1;

    if (0 <= rowIdx1 and rowIdx1 < m_handlersCount) {
      displayPanel.updateRow1(m_settingsHandlers[rowIdx1]->settingsValue());
    }
    else {
      displayPanel.clearRow1();
    }

    if (0 <= rowIdx2 and rowIdx2 < m_handlersCount) {
      displayPanel.updateRow2(m_settingsHandlers[rowIdx2]->settingsValue());
    }
    else {
      displayPanel.clearRow2();
    }

    if (0 <= rowIdx3 and rowIdx3 < m_handlersCount) {
      displayPanel.updateRow3(m_settingsHandlers[rowIdx3]->settingsValue());
    }
    else {
      displayPanel.clearRow3();
    }
  }

}
