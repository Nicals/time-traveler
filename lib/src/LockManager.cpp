#include <ttraveler/LockManager.h>


namespace libtraveler
{

  LockManager::LockManager(EventBuffer& eventBuffer, Timer& timer, DisplayPanel& displayPanel)
    : m_eventBuffer{eventBuffer},
      m_timer{timer},
      m_displayPanel{displayPanel}
  {
    m_timer.setObserver(this);
  }

  void LockManager::onError()
  {
    m_timer.start();
    m_eventBuffer.lock();
    m_displayPanel.updateButtonState(DisplayPanel::RedButton, DisplayPanel::RedButton);
  }

  void LockManager::onSuccess()
  {
    m_timer.start();
    m_eventBuffer.lock();
    m_displayPanel.updateButtonState(DisplayPanel::ControlButton, DisplayPanel::RedButton);
  }

  void LockManager::onTimeout()
  {
    m_eventBuffer.unlock();
    m_displayPanel.updateButtonState(DisplayPanel::None, DisplayPanel::RedButton | DisplayPanel::ControlButton);
  }

}
