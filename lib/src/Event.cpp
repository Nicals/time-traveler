#include <ttraveler/Event.h>


namespace libtraveler
{

  EventBuffer::EventBuffer(void (*overflowListener)())
    : m_overflowListener{overflowListener}
  {
  }

  void EventBuffer::pushKeyboardInput(uint8_t key)
  {
    pushEvent({Event::KeyboardPressed, key});
  }

  void EventBuffer::pushButtonInput(Event::Button button)
  {
    pushEvent({Event::ButtonPressed, button});
  }

  void EventBuffer::pushNewDateInput()
  {
    pushEvent({Event::TimeWentOn});
  }

  void EventBuffer::pushShutdown()
  {
    pushEvent({Event::ShutdownTriggered});
  }

  Event EventBuffer::popEvent()
  {
    // buffer is empty
    if (m_tail == m_head and !m_willOverflow) {
      return {Event::NoEvent};
    }

    m_willOverflow = false;

    auto input = m_buffer[m_tail];
    m_tail = (m_tail + 1) % m_ringSize;

    return input;
  }

  void EventBuffer::lock()
  {
    m_locked = true;
  }

  void EventBuffer::unlock()
  {
    m_locked = false;
  }

  uint8_t EventBuffer::size() const
  {
    return m_ringSize;
  }

  void EventBuffer::pushEvent(Event input)
  {
    if (m_locked) {
      return;
    }

    // next write has been detected as overflow, input is dropped
    if (m_willOverflow) {
      if (m_overflowListener != nullptr) {
        m_overflowListener();
      }

      return;
    }

    auto newHead = (m_head + 1) % m_ringSize;

    // overflow detection on next write
    if (newHead == m_tail) {
      m_willOverflow = true;
    }

    m_buffer[m_head] = input;
    m_head = newHead;
  }

}
