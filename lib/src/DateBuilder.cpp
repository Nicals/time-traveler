#include <ttraveler/DateBuilder.h>


namespace libtraveler
{

  bool DateBuilder::push(uint8_t input)
  {
    // years
    if (m_currentDigit == 0) {
      m_date.m_year = input * 1000;
    }
    else if (m_currentDigit == 1) {
      m_date.m_year += input * 100;
    }
    else if (m_currentDigit == 2) {
      m_date.m_year += input * 10;
    }
    else if (m_currentDigit == 3) {
      m_date.m_year += input;
    }
    // month
    else if (m_currentDigit == 4) {
      if (input > 1) {
        return false;
      }
      m_date.m_month = input * 10;
    }
    else if (m_currentDigit == 5) {
      auto month = m_date.m_month + input;
      if (not (1 <= month and month <= 12)) {
        return false;
      }

      m_date.m_month = month;
    }
    // day
    else if (m_currentDigit == 6) {
      if (input > 3) {
        return false;
      }
      m_date.m_day = input * 10;
    }
    else if (m_currentDigit == 7) {
      auto day = m_date.m_day + input;
      if (not (1 <= day and day <= 31)) {
        return false;
      }

      m_date.m_day = day;
    }
    // hours
    else if (m_currentDigit == 8) {
      if (input > 5) {
        return false;
      }
      m_date.m_hours = input * 10;
    }
    else if (m_currentDigit == 9) {
      m_date.m_hours += input * 1;
    }
    // minutes
    else if (m_currentDigit == 10) {
      if (input > 5) {
        return false;
      }
      m_date.m_minutes = input * 10;
    }
    else if (m_currentDigit == 11) {
      m_date.m_minutes += input * 1;
    }
    else {
      return false;
    }

    m_currentDigit++;

    return true;
  }

  void DateBuilder::clear()
  {
    m_currentDigit = 0;
  }

  Date DateBuilder::getDate(const Date& dateTemplate) const
  {
    return Date{
      m_currentDigit > 3 ? m_date.m_year : dateTemplate.m_year,
      m_currentDigit > 5 ? m_date.m_month : dateTemplate.m_month,
      m_currentDigit > 7 ? m_date.m_day : dateTemplate.m_day,
      m_currentDigit > 9 ? m_date.m_hours : dateTemplate.m_hours,
      m_currentDigit > 11 ? m_date.m_minutes : dateTemplate.m_minutes,
    };
  }

  bool DateBuilder::hasContent() const
  {
    return m_currentDigit > 3;
  }

}
