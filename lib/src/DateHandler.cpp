#include <ttraveler/DateHandler.h>


namespace libtraveler
{

  DateHandler::DateHandler(const Date& dest, const Date& current, const Date& previous)
    : m_dest{dest},
      m_current{current},
      m_previous{previous}
  {
  }

  void DateHandler::setDest(const Date& dest)
  {
    m_dest = dest;
  }

  void DateHandler::setCurrent(const Date& current)
  {
    m_current = current;
  }

  const Date& DateHandler::dest() const
  {
    return m_dest;
  }

  const Date& DateHandler::current() const
  {
    return m_current;
  }

  const Date& DateHandler::previous() const
  {
    return m_previous;
  }

  void DateHandler::swapDest()
  {
    auto bufferDate = m_dest;
    m_dest = m_previous;
    m_previous = bufferDate;
  }

}
