#include <ttraveler/Alarm.h>


namespace libtraveler
{

  Alarm::Alarm(DisplayPanel& displayPanel)
    : m_displayPanel{displayPanel}
  {
  }

  bool Alarm::checkTriggerState(const DateHandler& dateHandler)
  {
    if (m_isTriggered) {
      return false;
    }

    auto curr = dateHandler.current();
    auto dest = dateHandler.dest();

    bool shouldTrigger = 
      curr.year() == dest.year()
      and curr.month() == dest.month()
      and curr.day() == dest.day()
      and curr.hours() == dest.hours()
      and curr.minutes() == dest.minutes()
      and curr.seconds() == 0;

    if (shouldTrigger) {
      trigger();
      return true;
    }

    return false;
  }

  void Alarm::trigger()
  {
    if (m_isTriggered) {
      return;
    }

    m_displayPanel.updateButtonState(DisplayPanel::RedButton, DisplayPanel::RedButton);
    m_isTriggered = true;
  }

  void Alarm::stop()
  {
    if (!m_isTriggered) {
      return;
    }

    m_displayPanel.updateButtonState(DisplayPanel::None, DisplayPanel::RedButton);
    m_isTriggered = false;
  }

  bool Alarm::isTriggered() const
  {
    return m_isTriggered;
  }

}
