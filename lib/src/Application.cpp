#include <ttraveler/Application.h>
#include <ttraveler/Clock.h>
#include <ttraveler/DateHandler.h>
#include <ttraveler/DisplayPanel.h>
#include <ttraveler/Event.h>


namespace libtraveler
{

  Application::Application(Clock& clock, EventBuffer& eventBuffer, DateHandler& dateHandler, DisplayPanel& displayPanel, Timer& lockTimer)
    : m_clock{clock},
      m_dateHandler{dateHandler},
      m_displayPanel{displayPanel},
      m_eventBuffer{eventBuffer},
      m_lockTimer{lockTimer},
      m_alarm{displayPanel},
      m_lockManager{m_eventBuffer, m_lockTimer, m_displayPanel},
      m_timeTravelMode{dateHandler},
      m_settingsMode{m_settings}
  {
    m_foregroundMode = &m_timeTravelMode;
    m_backgroundMode = &m_settingsMode;
  }

  Settings& Application::settings()
  {
    return m_settings;
  }

  bool Application::tick()
  {
    Event event;
    bool running = true;

    m_clock.update();

    while ((event = m_eventBuffer.popEvent()).type != Event::NoEvent) {
      if (event.type == Event::ShutdownTriggered) {
        running = false;
      }
      else if (event.type == Event::TimeWentOn) {
        m_alarm.checkTriggerState(m_dateHandler);
      }
      else if (event.type == Event::ButtonPressed) {
        if (event.button == Event::Button::Red) {
          if (m_alarm.isTriggered()) {
            m_alarm.stop();
          }
        }
        else if (event.button == Event::Button::Control) {
          switchModes();
        }
      }

      auto modeResult = m_foregroundMode->onInput(event, m_displayPanel);

      if (modeResult == Mode::Success) {
        m_lockManager.onSuccess();
      }
      else if (modeResult == Mode::Failure) {
        m_lockManager.onError();
      }
    }

    m_lockTimer.checkTimeout();

    return running;
  }

  void Application::switchModes()
  {
    auto tempMode = m_foregroundMode;
    m_foregroundMode = m_backgroundMode;
    m_backgroundMode = tempMode;

    m_foregroundMode->onEnter(m_displayPanel);
  }

}
