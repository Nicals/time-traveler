#include <ttraveler/Clock.h>


namespace libtraveler
{

  Clock::Clock(DateHandler& dateHandler, EventBuffer& eventBuffer)
    : m_dateHandler{dateHandler},
      m_eventBuffer{eventBuffer}
  {
  }

  void Clock::setDate(const Date& date, bool notify)
  {
    m_dateHandler.setCurrent(date);

    if (notify) {
      m_eventBuffer.pushNewDateInput();
    }
  }

  const DateHandler& Clock::dateHandler() const
  {
    return m_dateHandler;
  }

}
