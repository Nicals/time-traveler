#include <ttraveler/Date.h>


namespace libtraveler
{

  Date::Date(uint16_t year, uint8_t month, uint8_t day, uint8_t hours, uint8_t minutes, uint8_t seconds)
    : m_year{year},
      m_month{month},
      m_day{day},
      m_hours{hours},
      m_minutes{minutes},
      m_seconds{seconds}
  {
    if (m_year > 9999) {
      m_year = 9999;
    }

    if (m_month > 12) {
      m_month = 12;
    }
    else if (m_month < 1) {
      m_month = 1;
    }

    if (m_day > 31) {
      m_day = 28;
    }
    else if (m_day < 1) {
      m_day = 1;
    }

    if (m_hours > 23) {
      m_hours = 23;
    }
    if (m_minutes > 59) {
      m_minutes = 59;
    }

    if (m_seconds > 59) {
      m_seconds = 59;
    }
  }

  uint16_t Date::year() const
  {
    return m_year;
  }

  uint8_t Date::month() const
  {
    return m_month;
  }

  uint8_t Date::day() const
  {
    return m_day;
  }

  uint8_t Date::hours() const
  {
    return m_hours;
  }

  Date::Hours12Format Date::hours12() const
  {
    if (m_hours <= 12) {
      return {m_hours, true};
    }

    return {m_hours - uint8_t{12}, false};
  }

  uint8_t Date::minutes() const
  {
    return m_minutes;
  }

  uint8_t Date::seconds() const
  {
    return m_seconds;
  }

  bool Date::operator==(const Date& other) const
  {
    return (
        m_year == other.m_year
        and m_month == other.m_month
        and m_day == other.m_day
        and m_hours == other.m_hours
        and m_minutes == other.m_minutes
        and m_seconds == other.m_seconds
    );
  }

  bool Date::operator!=(const Date& other) const
  {
    return not (*this == other);
  }

}
