#pragma once

#include <ttraveler/DisplayPanel.h>
#include <ttraveler/Event.h>


namespace libtraveler
{

  /**
   * Base class for time travel machine operation modes
   */
  class Mode
  {
    public:
      enum InputResult
      {
        // Nothing to report
        None,
        // Input successfully consumed, no operation performed yet
        Ok,
        // An operatin was successfully performed
        Success,
        // An error occured
        Failure,
      };

      // called when the mode is activated
      virtual void onEnter(DisplayPanel& displayPanel) = 0;

      // called when a user input is received
      virtual InputResult onInput(Event event, DisplayPanel& displayPanel) = 0;
  };

}
