#pragma once

#include <ttraveler/DateBuilder.h>
#include <ttraveler/DateHandler.h>
#include <ttraveler/modes/Mode.h>


namespace libtraveler
{

  /**
   * Mode of operation that effectively control time travel.
   *
   * This mode is used to configure destination dates.
   */
  class TimeTravelMode: public Mode
  {
    public:
      TimeTravelMode(DateHandler& dateHandler);

      virtual void onEnter(DisplayPanel& displayPanel) override final;
      virtual Mode::InputResult onInput(Event input, DisplayPanel& displayPanel) override final;

    private:
      Mode::InputResult swapDestDate(DisplayPanel& displayPanel);
      Mode::InputResult injectDateInput(Event::Key key);
      Mode::InputResult setDestDate(DisplayPanel& displayPanel);
      Mode::InputResult setCurrentDate(DisplayPanel& displayPanel);

      DateBuilder m_dateBuilder;
      DateHandler& m_dateHandler;
  };

}
