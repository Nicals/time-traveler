#pragma once

#include <ttraveler/modes/Mode.h>


namespace libtraveler
{

  class SettingsValue;

  /**
   * Base class used to set settings value from user input
   */
  class SettingsValueHandler
  {
    public:
      SettingsValueHandler(SettingsValue& settingsValue);

      const SettingsValue& settingsValue() const;

      virtual void clearState() {};
      virtual Mode::InputResult consumeInput(Event input) = 0;

    protected:
      SettingsValue& m_settingsValue;
  };


  class BoolValueHandler: public SettingsValueHandler
  {
    public:
      BoolValueHandler(SettingsValue& settingsValue);

      Mode::InputResult consumeInput(Event input) override final;
  };


  class IntValueHandler: public SettingsValueHandler
  {
    public:
      IntValueHandler(SettingsValue& settings);
      ~IntValueHandler() {}

      void clearState() override final;
      Mode::InputResult consumeInput(Event input) override final;

    private:
      int m_buffer = 0;
  };
}
