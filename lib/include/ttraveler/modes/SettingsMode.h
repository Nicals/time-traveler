#pragma once

#include <ttraveler/modes/Mode.h>
#include <ttraveler/modes/SettingsHandler.h>


namespace libtraveler
{

  class Settings;

  class SettingsMode: public Mode
  {
    public:
      SettingsMode(Settings& settings);

      virtual void onEnter(DisplayPanel& displayPanel) override final;
      virtual Mode::InputResult onInput(Event input, DisplayPanel& displayPanel) override final;

    private:
      void refreshDisplay(DisplayPanel& displayPanel) const;

      Settings& m_settings;

      BoolValueHandler m_soundEnabledHandler;
      IntValueHandler m_brightnessHandler;

      int m_currentHandlerIndex = 0;
      static const int m_handlersCount = 2;
      SettingsValueHandler* m_settingsHandlers[m_handlersCount];
  };
}
