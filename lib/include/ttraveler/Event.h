#pragma once

#include <stdint.h>

#include <ttraveler/Date.h>


namespace libtraveler
{

  /**
   * Defines an event
   */
  class Event
  {
    public:
      enum EventType
      {
        // Nothing, void, emptyness, whatever... Nothing happened.
        NoEvent,
        // A keyboard key was pressed. Key value in Event::key
        KeyboardPressed,
        // A push button was pressed. Button pressed in Event::button
        ButtonPressed,
        // Current date has changed.
        TimeWentOn,
        // This event is intended to stop all operations
        ShutdownTriggered,
      };

      /**
       * See /docs/operations.md for details on those values
       */
      enum Button
      {
        Red,
        Green,
        Yellow,
        Control,
        Enter
      };

      using Key = uint8_t;

      EventType type;

      union
      {
        // The keypoad key that was pressed
        Key key;
        // The button that was pressed
        Button button;
      };
  };


  class EventBuffer
  {
    public:
      /**
       * Constructor
       *
       * An overflowListener pointer function will be called whenever an overflow
       * in internal buffer is detected
       */
      EventBuffer(void (*overflowListener)() = nullptr);

      // Push operations.
      // Pushing an event on a full buffer will drop the new input.
      // If an overflow listener function has been passed to the constructor,
      // it will be called whenever an overflow is detected.
      void pushKeyboardInput(Event::Key key);
      void pushButtonInput(Event::Button button);
      void pushNewDateInput();
      void pushShutdown();

      Event popEvent();

      void lock();
      void unlock();

      uint8_t size() const;

    private:
      void pushEvent(Event event);

      // When locked, no new event are accepted. 
      bool m_locked = false;

      // ring buffer to store events. Since we expect to pop them frequently,
      // we do not need a such a big array.
      static const uint8_t m_ringSize = 5;
      Event m_buffer[m_ringSize];
      uint8_t m_tail = 0;
      uint8_t m_head = 0;

      // a function pointer to call when the internal buffer overflows.
      void (*m_overflowListener)() = nullptr;
      bool m_willOverflow = false;
  };

}
