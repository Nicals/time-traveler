#pragma once

#include <ttraveler/DisplayPanel.h>
#include <ttraveler/Event.h>
#include <ttraveler/Timer.h>


namespace libtraveler
{

  class LockManager: public TimerObserver
  {
    public:
      LockManager(EventBuffer& eventBuffer, Timer& timer, DisplayPanel& displayPanel);

      void onError();
      void onSuccess();

      void onTimeout() override;

    private:
      EventBuffer& m_eventBuffer;
      Timer& m_timer;
      DisplayPanel& m_displayPanel;
  };

}
