#pragma once

#include <ttraveler/Date.h>


namespace libtraveler
{

  /**
   * Handles the 3 dates used by the system: Destination date, Current date
   * and previous date.
   */
  class DateHandler
  {
    public:
      DateHandler(const Date& dest, const Date& current, const Date& previous);

      void setDest(const Date& dest);
      void setCurrent(const Date& current);

      const Date& dest() const;
      const Date& current() const;
      const Date& previous() const;

      // swap destination date with previous date
      void swapDest();

    private:
      Date m_dest, m_current, m_previous;
  };

}
