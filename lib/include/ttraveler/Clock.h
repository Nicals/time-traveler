#pragma once

#include <ttraveler/DateHandler.h>
#include <ttraveler/Event.h>


namespace libtraveler
{

  /**
   * Use to manage passing time.
   *
   * An internal 'new date' flag is used to detect whenever the current date has
   * changed.
   */
  class Clock
  {
    public:
      Clock(DateHandler& dateHandler, EventBuffer& eventBuffer);

      /**
       * Check if the current date has changed.
       *
       * If so, it is the overriden method should call Clock:setDate with the
       * new method and notify argument set to true
       */
      virtual void update() = 0;

      /**
       * Set the current date.
       *
       * Pushes an Event::TimeWentOn event in the event buffer with the new
       * date.
       *
       * If the notify args is set to false, the new date will be set without
       * pushing events into the event buffer.
       */
      void setDate(const Date& date, bool notify = true);

    protected:
      const DateHandler& dateHandler() const;

    private:
      DateHandler& m_dateHandler;
      EventBuffer& m_eventBuffer;
  };

}
