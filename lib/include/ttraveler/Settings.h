#pragma once


namespace libtraveler
{

  class SettingsValue
  {
    public:
      SettingsValue(const char* name, int initial = 100, int max = 100);

      const char* name() const;

      int value() const;
      void setValue(int value);

      int maxValue() const;

      // only needed for gtest
      bool operator==(const SettingsValue& other) const
      {
        for (int i = 0 ; i < 3 ; i++) {
          if (other.m_name[i] != m_name[i]) {
            return false;
          }
        }

        return true;
      }

    private:
      char m_name[3];

      int m_value;
      int m_maxValue;
  };

  class Settings
  {
    public:
      Settings();

      SettingsValue& soundEnabled();
      SettingsValue& brightness();

    private:
      SettingsValue m_soundEnabled;
      SettingsValue m_brightness;
  };

}
