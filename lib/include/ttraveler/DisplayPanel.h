#pragma once

#include <stdlib.h>

#include <ttraveler/Date.h>
#include <ttraveler/Settings.h>


namespace libtraveler
{

  class DisplayPanel
  {
    public:
      virtual ~DisplayPanel() {}

      enum
      {
        None = 0,
        RedButton = 1 << 0,
        GreenButton = 1 << 1,
        YellowButton = 1 << 2,
        EnterButton = 1 << 3,
        ControlButton = 1 << 4,
        AllButtons = (1 << 5) - 1,
      };

      virtual void clearRow1() = 0;
      virtual void clearRow2() = 0;
      virtual void clearRow3() = 0;

      virtual void updateRow1(const Date& date) = 0;
      virtual void updateRow2(const Date& date) = 0;
      virtual void updateRow3(const Date& date) = 0;

      virtual void updateRow1(const SettingsValue& value) = 0;
      virtual void updateRow2(const SettingsValue& value) = 0;
      virtual void updateRow3(const SettingsValue& value) = 0;

      using ButtonState = uint8_t;

      /**
       * Update buttons light state. Button lights bits set to 1 will be turned
       * on and turned of on 0.
       *
       * Only buttons set in mask will be updated, all other keeps there current
       * state.
       */
      virtual void updateButtonState(ButtonState buttons, ButtonState mask = AllButtons) = 0;
  };

}
