#pragma once


namespace libtraveler
{

  class TimerObserver
  {
    public:
      virtual void onTimeout() = 0;
  };


  class Timer
  {
    public:
      virtual ~Timer() {}

      /**
       * Starts the timer.
       * If already started, won't do anything
       */
      void start();

      /**
       * Checks if the timer has timed out and calls the observer onTimeout
       * method.
       *
       * Timeout is checked by calling the hasTimedOut method.
       */
      void checkTimeout();

      /**
       * Set a timer observer that will be called on timeout, replacing any
       * previous observer
       */
      void setObserver(TimerObserver* observer);

    protected:
      /**
       * Called when the timer is started.
       *
       * Override this method to initialize the timer
       */
      virtual void onStart() = 0;

      /**
       * Tells if a started timer has timed out
       */
      virtual bool hasTimedOut() = 0;

      TimerObserver* m_observer = nullptr;
      bool m_started = false;
  };

}
