#pragma once

#include <ttraveler/Alarm.h>
#include <ttraveler/LockManager.h>
#include <ttraveler/modes/SettingsMode.h>
#include <ttraveler/modes/TimeTravelMode.h>
#include <ttraveler/Settings.h>


namespace libtraveler
{

  class Clock;
  class DateHandler;
  class DisplayPanel;
  class EventBuffer;
  class Timer;

  class Application
  {
    public:
      Application(Clock& clock, EventBuffer& eventBuffer, DateHandler& dateHandler, DisplayPanel& displayPanel, Timer& lockTimer);

      Settings& settings();

      // will return true if the application should keep running
      bool tick();

    private:
      Clock& m_clock;
      DateHandler& m_dateHandler;
      DisplayPanel& m_displayPanel;
      EventBuffer& m_eventBuffer;
      Timer& m_lockTimer;

      Alarm m_alarm;
      LockManager m_lockManager;

      Settings m_settings;

      TimeTravelMode m_timeTravelMode;
      SettingsMode m_settingsMode;

      void switchModes();

      Mode* m_foregroundMode;
      Mode* m_backgroundMode;
  };

}
