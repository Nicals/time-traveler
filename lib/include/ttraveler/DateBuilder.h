#pragma once

#include <stdint.h>

#include <ttraveler/Date.h>


namespace libtraveler
{

  /**
   * Builds dates by consuming [0-9] integer, one at a time.
   *
   * Dates are built from year down to minutes. Seconds are alway set to 0
   */
  class DateBuilder
  {
    public:
      /**
       * Add an [0-9] integer to the date being built.
       * If the value is not valid to for the current date state, it is discarded
       * and false is return.
       *
       * Else, the date is updated and true is returned.
       */
      bool push(uint8_t input);
      /**
       * Clear the current date being built.
       */
      void clear();

      /**
       * Get the date from current builder state.
       *
       * If any value is missing (ex: user has only entered year and month),
       * any values not set will be taken from the date template.
       */
      Date getDate(const Date& dateTemplate) const;

      /**
       * Returns true if some valid content has been pushed.
       *
       * Valid content assumes at least a complete date
       */
      bool hasContent() const;

    private:
      // The date being built
      Date m_date;

      // The next digit to enter
      uint8_t m_currentDigit = 0;
  };

}
