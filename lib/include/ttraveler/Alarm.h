#pragma once

#include <ttraveler/DateHandler.h>
#include <ttraveler/DisplayPanel.h>


namespace libtraveler
{

  class Alarm
  {
    public:
      Alarm(DisplayPanel& displayPanel);

      /**
       * Check if the date handler is in an alarm triggering state.
       * If so, the alarm is triggered and true is returned.
       *
       * If the state of the date handler does not calls for an alarm trigger,
       * false is returned and nothing more is done.
       *
       * If the alarm is already in a triggered state, returns false and nothing
       * more is done.
       */
      bool checkTriggerState(const DateHandler& dateHandler);

      /**
       * Stop the alarm if triggered.
       *
       * If not, nothing is actually done
       */
      void stop();

      bool isTriggered() const;

    private:
      void trigger();

      bool m_isTriggered = false;
      DisplayPanel& m_displayPanel;
  };

}
