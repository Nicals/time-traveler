#pragma once

#include <stdint.h>


namespace libtraveler
{

  class DateBuilder;

  class Date
  {
    public:
      struct Hours12Format
      {
        uint8_t hours;
        bool am;
      };

      Date(
          uint16_t year = 1985,
          uint8_t month = 1,
          uint8_t day = 1,
          uint8_t hours = 0,
          uint8_t minutes = 0,
          uint8_t seconds = 0
      );

      // Accessors
      uint8_t seconds() const;
      uint8_t minutes() const;
      // Get hours in 24 hours format
      uint8_t hours() const;
      // Get hours in 12 hours format
      Hours12Format hours12() const;

      uint8_t day() const;
      uint8_t month() const;
      uint16_t year() const;

      bool operator==(const Date& other) const;
      bool operator!=(const Date& other) const;

    private:
      friend class DateBuilder;

      uint16_t m_year;
      uint8_t m_month;
      uint8_t m_day;

      uint8_t m_hours;
      uint8_t m_minutes;
      uint8_t m_seconds;
  };

}
