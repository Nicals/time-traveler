#include <ncurses.h>
#include <ttraveler/Application.h>
#include <ttraveler/DateHandler.h>

#include "Clock.h"
#include "DisplayPanel.h"
#include "Timer.h"


void readInput(int inputKey, libtraveler::EventBuffer& eventBuffer)
{
  if (inputKey == 'a') {
    eventBuffer.pushButtonInput(libtraveler::Event::Red);
  }
  else if (inputKey == 'z') {
    eventBuffer.pushButtonInput(libtraveler::Event::Green);
  }
  else if (inputKey == 'e') {
    eventBuffer.pushButtonInput(libtraveler::Event::Yellow);
  }
  // For some reason, curses KEY_ENTER is defined as 0527.
  // I do not know why that is, so we match the char directly
  else if (inputKey == '\n') {
    eventBuffer.pushButtonInput(libtraveler::Event::Enter);
  }
  else if (inputKey == 'c') {
    eventBuffer.pushButtonInput(libtraveler::Event::Control);
  }
  else if ('0' <= inputKey and inputKey <= '9') {
    eventBuffer.pushKeyboardInput(inputKey - '0');
  }
  else if (inputKey == 'q') {
    eventBuffer.pushShutdown();
  }
}


int main(int argc, char* argv[])
{
  initscr();
  cbreak();
  // Don't display input keys
  noecho();
  // Non-blocking input
  timeout(10);
  // Hide cursor
  curs_set(0);
  start_color();

  libtraveler::EventBuffer eventBuffer;
  libtraveler::DateHandler dateHandler{
    {1955, 10, 26, 1, 21},
    {1985, 10, 26, 1, 22},
    {1985, 10, 26, 1, 20}
  };
  CursesTraveler::DisplayPanel displayPanel;
  displayPanel.updateRow1(dateHandler.dest());
  displayPanel.updateRow2(dateHandler.current());
  displayPanel.updateRow3(dateHandler.previous());

  CursesTraveler::Timer lockTimer;

  CursesTraveler::Clock clock{dateHandler, eventBuffer};

  libtraveler::Application app{clock, eventBuffer, dateHandler, displayPanel, lockTimer};

  bool running = true;

  while (running) {
    readInput(getch(), eventBuffer);

    running = app.tick();

    displayPanel.display();
    refresh();
  }

  endwin();

  return EXIT_SUCCESS;
}
