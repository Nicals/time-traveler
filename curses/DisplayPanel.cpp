#include <iomanip>
#include <sstream>

#include <ncurses.h>

#include "DisplayPanel.h"


namespace CursesTraveler
{

  DisplayPanel::DisplayPanel()
  {
    init_pair(m_redPair, COLOR_RED, COLOR_BLACK);
    init_pair(m_greenPair, COLOR_GREEN, COLOR_BLACK);
    init_pair(m_yellowPair, COLOR_YELLOW, COLOR_BLACK);
    init_pair(m_whitePair, COLOR_WHITE, COLOR_BLACK);
  }

  void DisplayPanel::clearRow1()
  {
    m_row1 = "           :  :  ";
  }

  void DisplayPanel::clearRow2()
  {
    m_row2 = "           :  :  ";
  }

  void DisplayPanel::clearRow3()
  {
    m_row3 = "           :  :  ";
  }

  void DisplayPanel::updateRow1(const libtraveler::Date& date)
  {
    m_row1 = formatRow(date);
  }

  void DisplayPanel::updateRow2(const libtraveler::Date& date)
  {
    m_row2 = formatRow(date);
  }

  void DisplayPanel::updateRow3(const libtraveler::Date& date)
  {
    m_row3 = formatRow(date);
  }

  void DisplayPanel::updateRow1(const libtraveler::SettingsValue& settings)
  {
    m_row1 = formatRow(settings);
  }

  void DisplayPanel::updateRow2(const libtraveler::SettingsValue& settings)
  {
    m_row2 = formatRow(settings);
  }

  void DisplayPanel::updateRow3(const libtraveler::SettingsValue& settings)
  {
    m_row3 = formatRow(settings);
  }

  std::string DisplayPanel::formatRow(const libtraveler::Date& date)
  {
    std::stringstream rowStream;
    rowStream << std::fixed << std::setfill('0');

    if (0 <= date.month() and date.month() <= 12) {
      rowStream << m_verboseMonth[date.month() - 1];
    }
    else
    {
      rowStream << "ERR";
    }

    rowStream
      << " " << std::setw(2) << (int)date.day()
      << " " << std::setw(4) << (int)date.year()
      << ":"  // AM/PM indicator
      << std::setw(2) << (int)date.hours() << ":" << (int)date.minutes();

    return rowStream.str();
  }

  std::string DisplayPanel::formatRow(const libtraveler::SettingsValue& settings)
  {
    std::stringstream rowStream;

    rowStream
      << std::fixed
      << settings.name()[0] << settings.name()[1] << settings.name()[2]
      << "   " << std::setw(4) << settings.value()
      << "      ";

    return rowStream.str();
  }

  void DisplayPanel::updateButtonState(ButtonState buttons, ButtonState mask)
  {
    m_buttonState &= ~mask;
    m_buttonState |= buttons;
  }

  void DisplayPanel::display() const
  {
    const int buttonRow = 1;
    const int buttonHintWidth = 12;
    const int buttonCol = 2;

    const int dateRow = 1;
    const int dateCol = buttonCol + 17;

    // buttons
    attron(COLOR_PAIR(m_redPair));
    mvprintw(buttonRow + 0, buttonCol, "        a:");
    drawButton(buttonRow + 0, buttonCol + buttonHintWidth, m_buttonState & RedButton);

    attron(COLOR_PAIR(m_greenPair));
    mvprintw(buttonRow + 1, buttonCol, "        z:");
    drawButton(buttonRow + 1, buttonCol + buttonHintWidth, m_buttonState & GreenButton);

    attron(COLOR_PAIR(m_yellowPair));
    mvprintw(buttonRow + 2, buttonCol, "        e:");
    drawButton(buttonRow + 2, buttonCol + buttonHintWidth, m_buttonState & YellowButton);

    attron(COLOR_PAIR(m_whitePair));
    mvprintw(buttonRow + 3, buttonCol, "    enter:");
    drawButton(buttonRow + 3, buttonCol + buttonHintWidth, m_buttonState & EnterButton);

    mvprintw(buttonRow + 4, buttonCol, "        c:");
    drawButton(buttonRow + 4, buttonCol + buttonHintWidth, m_buttonState & ControlButton);

    // date rows
    attron(COLOR_PAIR(m_redPair));
    mvprintw(dateRow + 0, dateCol, m_row1.c_str());

    attron(COLOR_PAIR(m_greenPair));
    mvprintw(dateRow + 1, dateCol, m_row2.c_str());

    attron(COLOR_PAIR(m_yellowPair));
    mvprintw(dateRow + 2, dateCol, m_row3.c_str());

    attroff(COLOR_PAIR(2));
  }

  void DisplayPanel::drawButton(int row, int col, bool active) const
  {
    if (active) {
      attron(A_BOLD);
      mvprintw(row, col, "[x]");
      attroff(A_BOLD);
    }
    else {
      mvprintw(row, col, "[ ]");
    }
  }

}
