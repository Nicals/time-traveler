#include <chrono>
#include <ttraveler/Timer.h>


namespace CursesTraveler
{

  class Timer: public libtraveler::Timer
  {
    public:
      Timer();

    protected:
      void onStart() override;

      bool hasTimedOut() override;

    private:
      // time in ms from start to timeout
      const int m_delay = 1000;
      std::chrono::time_point<std::chrono::steady_clock> m_timerStartTime;
  };

}
