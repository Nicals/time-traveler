#pragma once

#include <string>

#include <ttraveler/DisplayPanel.h>


namespace CursesTraveler
{

  class DisplayPanel: public libtraveler::DisplayPanel
  {
    public:
      DisplayPanel();
      ~DisplayPanel() {}

      virtual void clearRow1() override final;
      virtual void clearRow2() override final;
      virtual void clearRow3() override final;

      virtual void updateRow1(const libtraveler::Date& date) override final;
      virtual void updateRow2(const libtraveler::Date& date) override final;
      virtual void updateRow3(const libtraveler::Date& date) override final;

      virtual void updateRow1(const libtraveler::SettingsValue& settings) override final;
      virtual void updateRow2(const libtraveler::SettingsValue& settings) override final;
      virtual void updateRow3(const libtraveler::SettingsValue& settings) override final;

      virtual void updateButtonState(ButtonState buttons, ButtonState mask = AllButtons) override final;

      void display() const;

    private:
      void drawButton(int row, int col, bool active) const;

      // curses color definition
      const int m_redPair = 1;
      const int m_greenPair = 2;
      const int m_yellowPair = 3;
      const int m_whitePair = 4;

      const std::string m_verboseMonth[12] = {
        "JAN",
        "FEB",
        "MAR",
        "APR",
        "MAY",
        "JUN",
        "JUL",
        "AGO",
        "SEP",
        "OCT",
        "NOV",
        "DEC",
      };

      std::string formatRow(const libtraveler::Date& date);
      std::string formatRow(const libtraveler::SettingsValue& settings);

      std::string m_row1;
      std::string m_row2;
      std::string m_row3;

      ButtonState m_buttonState = None;
  };

}
