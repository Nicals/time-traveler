#include <ttraveler/Clock.h>

#include <chrono>


namespace CursesTraveler
{
  class Clock: public libtraveler::Clock
  {
    public:
      Clock(libtraveler::DateHandler& dateHandler, libtraveler::EventBuffer& eventBuffer);

      void update() override;

    private:
      libtraveler::Date getCurrentDate() const;

      std::chrono::time_point<std::chrono::steady_clock> m_lastTimePoint;
  };

}
