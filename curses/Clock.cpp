#include "Clock.h"


namespace CursesTraveler
{

  Clock::Clock(libtraveler::DateHandler& dateHandler, libtraveler::EventBuffer& eventBuffer)
    : libtraveler::Clock{dateHandler, eventBuffer},
      m_lastTimePoint{std::chrono::steady_clock::now()}
  {
    setDate(getCurrentDate(), false);
  }

  libtraveler::Date Clock::getCurrentDate() const
  {
    std::time_t currentTime = std::chrono::system_clock::to_time_t(
      std::chrono::system_clock::now()
    );
    std::tm* tm = std::localtime(&currentTime);

    libtraveler::Date currentDate{
      tm->tm_year + 1900,
      tm->tm_mon + 1,
      tm->tm_mday,
      tm->tm_hour,
      tm->tm_min,
      tm->tm_sec,
    };

    return currentDate;
  }

  void Clock::update()
  {
    libtraveler::Date currentDate = getCurrentDate();

    if (currentDate != dateHandler().current()) {
      setDate(currentDate);
    }
  }

}
