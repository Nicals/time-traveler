#include "Timer.h"


namespace CursesTraveler
{

  Timer::Timer()
    : m_timerStartTime{std::chrono::steady_clock::now()}
  {
  }

  void Timer::onStart()
  {
    m_timerStartTime = std::chrono::steady_clock::now();
  }

  bool Timer::hasTimedOut()
  {
    auto elapsedTime = std::chrono::steady_clock::now() - m_timerStartTime;

    return std::chrono::duration_cast<std::chrono::milliseconds>(elapsedTime).count() > m_delay;
  }

}
