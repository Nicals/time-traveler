#include <gtest/gtest.h>

#include "mocks/ClockMock.h"


using namespace libtraveler;


TEST(ClockTest, TestNotifiesNewDates)
{
  EventBuffer eventBuffer;
  DateHandler dateHandler{Date{2000, 1, 1}, Date{2001, 1, 1}, Date{2002, 1, 1}};
  ClockMock clock{dateHandler, eventBuffer};
  Date date{1985, 10, 26};

  clock.setDate(date);

  Event event = eventBuffer.popEvent();
  EXPECT_EQ(event.type, Event::TimeWentOn);
  EXPECT_EQ(dateHandler.current(), date);
}


TEST(ClockTest, TestSetDateWithoutNotification)
{
  EventBuffer eventBuffer;
  DateHandler dateHandler{Date{2000, 1, 1}, Date{2001, 1, 1}, Date{2002, 1, 1}};
  ClockMock clock{dateHandler, eventBuffer};
  Date date{1985, 10, 26};

  clock.setDate(date, false);

  Event event = eventBuffer.popEvent();
  EXPECT_EQ(event.type, Event::NoEvent);
  EXPECT_EQ(dateHandler.current(), date);
}
