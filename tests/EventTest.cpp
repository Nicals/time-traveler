#include <gtest/gtest.h>

#include <ttraveler/Event.h>


using namespace libtraveler;


TEST(EventBufferTest, TestReadsInputUntilDepleted)
{
  EventBuffer reader;
  reader.pushKeyboardInput(12);
  reader.pushButtonInput(Event::Button::Green);

  auto input = reader.popEvent();

  EXPECT_EQ(input.type, Event::KeyboardPressed);
  EXPECT_EQ(input.key, 12);

  input = reader.popEvent();

  EXPECT_EQ(input.type, Event::ButtonPressed);
  EXPECT_EQ(input.button, Event::Button::Green);

  input = reader.popEvent();

  EXPECT_EQ(input.type, Event::NoEvent);
}


TEST(EventBufferTest, TestLocksAndUnlocksInput)
{
  EventBuffer reader;
  reader.lock();
  reader.pushKeyboardInput(12);

  auto input = reader.popEvent();

  EXPECT_EQ(input.type, Event::NoEvent);

  reader.unlock();
  reader.pushKeyboardInput(12);

  input = reader.popEvent();

  EXPECT_EQ(input.type, Event::KeyboardPressed);
}


TEST(EventBufferTest, TestDetectsOverflow)
{
  static bool hasOverflown = false;
  EventBuffer reader([]() { hasOverflown = true; });

  for (int i = 0 ; i < reader.size() ; i++) {
    reader.pushButtonInput(Event::Button::Green);
  }

  EXPECT_FALSE(hasOverflown) << "Overflow observer was called when it shouldn't";

  reader.pushButtonInput(Event::Button::Green);

  EXPECT_TRUE(hasOverflown) << "Overflow observer was not called";

  auto input = reader.popEvent();

  EXPECT_EQ(input.button, Event::Button::Green);
}
