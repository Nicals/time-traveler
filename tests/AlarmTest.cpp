#include <gtest/gtest.h>

#include <ttraveler/Alarm.h>

#include "mocks/DisplayPanelMock.h"


using ::testing::NiceMock;
using namespace libtraveler;


TEST(AlarmTest, TestTriggerOnAndOff)
{
  DateHandler dateHandler(
      Date{2021, 10, 14, 21, 13, 0},
      Date{2021, 10, 14, 21, 13, 0},
      Date{1985, 10, 26, 1, 22, 0}
  );
  DisplayPanelMock displayPanel;
  Alarm alarm{displayPanel};

  {
    ::testing::InSequence seq;

    EXPECT_CALL(displayPanel, updateButtonState(DisplayPanel::RedButton, DisplayPanel::RedButton));
    EXPECT_CALL(displayPanel, updateButtonState(DisplayPanel::None, DisplayPanel::RedButton));
  }

  alarm.checkTriggerState(dateHandler);
  EXPECT_TRUE(alarm.isTriggered());

  alarm.stop();
  EXPECT_FALSE(alarm.isTriggered());
}


TEST(AlarmTest, TestOnlyTriggerOnFirstSecond)
{
  DateHandler dateHandler(
      Date{2021, 10, 14, 21, 13, 0},
      Date{2021, 10, 14, 21, 13, 10},
      Date{1985, 10, 26, 1, 22, 0}
  );
  NiceMock<DisplayPanelMock> displayPanel;
  Alarm alarm{displayPanel};

  EXPECT_FALSE(alarm.checkTriggerState(dateHandler));
  EXPECT_FALSE(alarm.isTriggered());

  dateHandler.setCurrent(Date{2021, 10, 14, 21, 13, 0});
  EXPECT_TRUE(alarm.checkTriggerState(dateHandler));
  EXPECT_TRUE(alarm.isTriggered());
}


TEST(AlarmTest, TestWontTriggerTwice)
{
  DateHandler dateHandler(
      Date{2021, 10, 14, 21, 13, 0},
      Date{2021, 10, 14, 21, 13, 0},
      Date{1985, 10, 26, 1, 22, 0}
  );
  NiceMock<DisplayPanelMock> displayPanel;
  Alarm alarm{displayPanel};

  EXPECT_TRUE(alarm.checkTriggerState(dateHandler));
  EXPECT_FALSE(alarm.checkTriggerState(dateHandler));
}
