#include <gtest/gtest.h>

#include <ttraveler/DateBuilder.h>


using namespace libtraveler;


class DateBuilderTest: public ::testing::Test
{
  protected:
    void SetUp() override
    {
      builderYear.push(1);
      builderYear.push(9);
      builderYear.push(8);
      builderYear.push(5);

      builderMonth.push(1);
      builderMonth.push(9);
      builderMonth.push(8);
      builderMonth.push(5);

      builderMonth.push(1);
      builderMonth.push(0);

      builderDay.push(1);
      builderDay.push(9);
      builderDay.push(8);
      builderDay.push(5);

      builderDay.push(1);
      builderDay.push(0);

      builderDay.push(2);
      builderDay.push(6);
    }

    // A date builder with a valid year part
    DateBuilder builderYear;

    // A date builder with a valid month part
    DateBuilder builderMonth;

    // A date builder with a valid month and day part
    DateBuilder builderDay;
};


TEST_F(DateBuilderTest, TestBuildsDate)
{
  Date expectedDate{1985, 10, 26, 1, 23, 0};
  Date dateTpl;
  DateBuilder builder;

  EXPECT_TRUE(builder.push(2));
  EXPECT_TRUE(builder.push(0));
  EXPECT_TRUE(builder.push(2));
  EXPECT_TRUE(builder.push(1));

  builder.clear();

  EXPECT_TRUE(builder.push(1));
  EXPECT_TRUE(builder.push(9));
  EXPECT_TRUE(builder.push(8));
  EXPECT_TRUE(builder.push(5));

  EXPECT_TRUE(builder.push(1));
  EXPECT_TRUE(builder.push(0));

  EXPECT_TRUE(builder.push(2));
  EXPECT_TRUE(builder.push(6));

  EXPECT_TRUE(builder.push(0));
  EXPECT_TRUE(builder.push(1));

  EXPECT_TRUE(builder.push(2));
  EXPECT_TRUE(builder.push(3));

  EXPECT_FALSE(builder.push(1)) << "No more  digits to push";

  EXPECT_EQ(builder.getDate(dateTpl), expectedDate);
}


TEST_F(DateBuilderTest, TestValidatesMonthTooHigh)
{
  EXPECT_FALSE(builderYear.push(2)) << "First month digits > 1";
  EXPECT_TRUE(builderYear.push(1));
  EXPECT_FALSE(builderYear.push(3)) << "Second month digit results in > 12";

  EXPECT_TRUE(builderYear.push(2)) << "Month '12' should be valid";
}


TEST_F(DateBuilderTest, TestValidatesMonthTooLow)
{
  EXPECT_TRUE(builderYear.push(0));
  EXPECT_FALSE(builderYear.push(0)) << "Month '0' does not exist";

  EXPECT_TRUE(builderYear.push(1)) << "Month '01' should be valid";
}


TEST_F(DateBuilderTest, TestValidatesDayTooHigh)
{
  EXPECT_FALSE(builderMonth.push(4)) << "First day digit > 4";
  EXPECT_TRUE(builderMonth.push(3));
  EXPECT_FALSE(builderMonth.push(2)) << "Second day digit results in > 31";

  EXPECT_TRUE(builderMonth.push(1)) << "Day '31' should be valid";
}


TEST_F(DateBuilderTest, TestValidatesDayTooLow)
{
  EXPECT_TRUE(builderMonth.push(0)) << "First day digits can be '0'";
  EXPECT_FALSE(builderMonth.push(0)) << "Day cannot be '00'";
  EXPECT_TRUE(builderMonth.push(1)) << "Day can be '01'";
}


TEST_F(DateBuilderTest, TestHoursTooHigh)
{
  EXPECT_FALSE(builderDay.push(6)) << "First hours digit > 5";
  EXPECT_TRUE(builderDay.push(5));
}


TEST_F(DateBuilderTest, TestMinutesTooHigh)
{
  EXPECT_TRUE(builderDay.push(1));
  EXPECT_TRUE(builderDay.push(2));

  EXPECT_FALSE(builderDay.push(6)) << "First minutes digit > 5";
  EXPECT_TRUE(builderDay.push(5));
}


TEST_F(DateBuilderTest, TestDateTemplate)
{
  DateBuilder builder;
  Date dateTpl{1985, 10, 30, 12, 13, 14};

  EXPECT_TRUE(builder.push(2));
  EXPECT_TRUE(builder.push(0));
  EXPECT_TRUE(builder.push(1));
  EXPECT_TRUE(builder.push(5));

  Date expected{2015, 10, 30, 12, 13, 0};
  EXPECT_EQ(builder.getDate(dateTpl), expected);
}


TEST_F(DateBuilderTest, TestDetectsEmptyContent)
{
  DateBuilder builder;

  EXPECT_FALSE(builder.hasContent());

  EXPECT_TRUE(builder.push(1));
  EXPECT_TRUE(builder.push(9));
  EXPECT_FALSE(builder.hasContent());

  EXPECT_TRUE(builder.push(1));
  EXPECT_TRUE(builder.push(9));
  EXPECT_TRUE(builder.hasContent());
}
