#pragma once

#include <gmock/gmock.h>

#include <ttraveler/DisplayPanel.h>


using namespace libtraveler;


class DisplayPanelMock: public DisplayPanel
{
  public:
    MOCK_METHOD(void, clearRow1, (), (override));
    MOCK_METHOD(void, clearRow2, (), (override));
    MOCK_METHOD(void, clearRow3, (), (override));

    MOCK_METHOD(void, updateRow1, (const Date& date), (override));
    MOCK_METHOD(void, updateRow2, (const Date& date), (override));
    MOCK_METHOD(void, updateRow3, (const Date& date), (override));

    MOCK_METHOD(void, updateRow1, (const SettingsValue& value), (override));
    MOCK_METHOD(void, updateRow2, (const SettingsValue& value), (override));
    MOCK_METHOD(void, updateRow3, (const SettingsValue& value), (override));

    MOCK_METHOD(void, updateButtonState, (DisplayPanel::ButtonState buttons, DisplayPanel::ButtonState mask), (override));
};
