#pragma once

#include <gmock/gmock.h>

#include <ttraveler/Timer.h>


using namespace libtraveler;


class TimerImpl: public Timer
{
  public:
    bool isTimedOut = false;

  protected:
    virtual void onStart() override {}

    virtual bool hasTimedOut() override
    {
      return isTimedOut;
    }
};


class TimerObserverImpl: public TimerObserver
{
  public:
    bool isCalled() const
    {
      return m_called;
    }

    void reset()
    {
      m_called = false;
    }

    virtual void onTimeout() override
    {
      m_called = true;
    }

  private:
    bool m_called = false;
};
