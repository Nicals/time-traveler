#pragma once

#include <gmock/gmock.h>

#include <ttraveler/Clock.h>

using namespace libtraveler;


class ClockMock: public Clock
{
  public:
    ClockMock(DateHandler& dateHandler, EventBuffer& eventBuffer)
      : Clock{dateHandler, eventBuffer}
    {
    }

    MOCK_METHOD(void, update, (), (override));
};
