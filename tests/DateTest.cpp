#include <gtest/gtest.h>

#include <ttraveler/Date.h>


using namespace libtraveler;


TEST(DateTest, TestCleansHighInputs)
{
  Date date{
    10000, 14, 32,
    100, 100, 100
  };

  EXPECT_EQ(59, date.seconds());
  EXPECT_EQ(59, date.minutes());
  EXPECT_EQ(23, date.hours());

  EXPECT_EQ(28, date.day());
  EXPECT_EQ(12, date.month());
  EXPECT_EQ(9999, date.year());
}


TEST(DateTest, TestDateEquality)
{
  Date date1{1985, 10, 26, 1, 20, 0};
  Date date2{1985, 10, 26, 1, 20, 0};
  Date date3{2015, 10, 21, 7, 28, 0};

  EXPECT_TRUE(date1 == date2);
  EXPECT_TRUE(date1 != date3);
}


TEST(DateTest, TestCleansLowInputs)
{ 
  Date date{0, 0, 0, 0, 0, 0};

  EXPECT_EQ(0, date.seconds());
  EXPECT_EQ(0, date.minutes());
  EXPECT_EQ(0, date.hours());

  EXPECT_EQ(1, date.day());
  EXPECT_EQ(1, date.month());
  EXPECT_EQ(0, date.year());
}


TEST(DateTest, Test12HoursFormat)
{
  Date date{1985, 11, 5, 11};

  auto hours = date.hours12();

  EXPECT_EQ(11, hours.hours);
  EXPECT_TRUE(hours.am);

  date = Date{1985, 11, 5, 14};
  hours = date.hours12();

  EXPECT_EQ(2, hours.hours);
  EXPECT_FALSE(hours.am);
}
