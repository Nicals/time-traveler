#include <gtest/gtest.h>

#include <ttraveler/LockManager.h>

#include "mocks/DisplayPanelMock.h"
#include "mocks/TimerMock.h"


using namespace libtraveler;


TEST(LockManagerTest, TestLocksAndUnlocksInputOnError)
{
  EventBuffer eventBuffer;
  TimerImpl timer;
  DisplayPanelMock displayPanel;

  {
    ::testing::InSequence seq;

    EXPECT_CALL(displayPanel, updateButtonState(DisplayPanel::RedButton, DisplayPanel::RedButton));
    EXPECT_CALL(displayPanel, updateButtonState(DisplayPanel::None, DisplayPanel::RedButton | DisplayPanel::ControlButton));
  }

  LockManager lockManager(eventBuffer, timer, displayPanel);
  lockManager.onError();

  eventBuffer.pushKeyboardInput(1);
  auto input = eventBuffer.popEvent();
  EXPECT_EQ(input.type, Event::NoEvent) << "Event buffer should be locked";

  timer.isTimedOut = true;
  timer.checkTimeout();
  eventBuffer.pushKeyboardInput(2);
  input = eventBuffer.popEvent();
  EXPECT_EQ(input.type, Event::KeyboardPressed) << "Event buffer should be locked";
}


TEST(LockManagerTest, TestLocksAndUnlocksInputOnSuccess)
{
  EventBuffer eventBuffer;
  TimerImpl timer;
  DisplayPanelMock displayPanel;

  {
    ::testing::InSequence seq;

    EXPECT_CALL(displayPanel, updateButtonState(DisplayPanel::ControlButton, DisplayPanel::RedButton));
    EXPECT_CALL(displayPanel, updateButtonState(DisplayPanel::None, DisplayPanel::RedButton | DisplayPanel::ControlButton));
  }

  LockManager lockManager(eventBuffer, timer, displayPanel);
  lockManager.onSuccess();

  eventBuffer.pushKeyboardInput(1);
  auto input = eventBuffer.popEvent();
  EXPECT_EQ(input.type, Event::NoEvent) << "Event buffer should be locked";

  timer.isTimedOut = true;
  timer.checkTimeout();
  eventBuffer.pushKeyboardInput(2);
  input = eventBuffer.popEvent();
  EXPECT_EQ(input.type, Event::KeyboardPressed) << "Event buffer should be locked";
}
