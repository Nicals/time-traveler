#include <gtest/gtest.h>

#include <ttraveler/modes/SettingsHandler.h>
#include <ttraveler/Settings.h>


using namespace libtraveler;


TEST(BoolSettingsHandlerTest, TestToggleValue)
{
  SettingsValue value{"BOO", 0};
  BoolValueHandler hdl{value};
  Event event{Event::ButtonPressed, Event::Button::Green};

  auto result = hdl.consumeInput(event);
  EXPECT_EQ(result, Mode::Success);
  EXPECT_EQ(value.value(), 1);

  result = hdl.consumeInput(event);
  EXPECT_EQ(value.value(), 0);
  EXPECT_EQ(result, Mode::Success);
}


TEST(BoolSettingsHandlerTest, TestUnknownInput)
{
  SettingsValue value{"BOO", 0};
  BoolValueHandler hdl{value};
  Event event{Event::ButtonPressed, Event::Button::Red};

  auto result = hdl.consumeInput(event);
  EXPECT_EQ(result, Mode::None);
}


TEST(IntValueHandlerTest, TestSetsNumberTwice)
{
  SettingsValue value{"FOO", 0};
  IntValueHandler hdl{value};

  auto result = hdl.consumeInput({Event::KeyboardPressed, 4});
  EXPECT_EQ(result, Mode::None);

  result = hdl.consumeInput({Event::KeyboardPressed, 2});
  EXPECT_EQ(result, Mode::None);

  result = hdl.consumeInput({Event::ButtonPressed, Event::Button::Green});
  EXPECT_EQ(result, Mode::Success);
  EXPECT_EQ(value.value(), 42);

  result = hdl.consumeInput({Event::KeyboardPressed, 2});
  EXPECT_EQ(result, Mode::None);

  result = hdl.consumeInput({Event::ButtonPressed, Event::Button::Green});
  EXPECT_EQ(result, Mode::Success);
  EXPECT_EQ(value.value(), 2);
}


TEST(IntValueHandlerTest, TestClearInputBuffer)
{
  SettingsValue value{"FOO", 0};
  IntValueHandler hdl{value};

  auto result = hdl.consumeInput({Event::KeyboardPressed, 4});
  EXPECT_EQ(result, Mode::None);

  hdl.clearState();

  result = hdl.consumeInput({Event::KeyboardPressed, 2});
  EXPECT_EQ(result, Mode::None);

  result = hdl.consumeInput({Event::ButtonPressed, Event::Button::Green});
  EXPECT_EQ(result, Mode::Success);
  EXPECT_EQ(value.value(), 2);
}


TEST(IntValueHandlerTest, TestValueOverflow)
{
  SettingsValue value{"FOO", 0, 40};
  IntValueHandler hdl{value};

  auto result = hdl.consumeInput({Event::KeyboardPressed, 4});
  EXPECT_EQ(result, Mode::None);

  result = hdl.consumeInput({Event::KeyboardPressed, 2});
  EXPECT_EQ(result, Mode::Failure);
}
