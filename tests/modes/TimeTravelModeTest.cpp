#include <gtest/gtest.h>

#include <ttraveler/modes/TimeTravelMode.h>

#include "../mocks/DisplayPanelMock.h"


using namespace libtraveler;


TEST(TimeTravelModeTest, TestNothingToDo)
{
  Date dest{1955, 10, 26};
  Date curr{2021, 10, 17};
  Date prev{1985, 100, 26};
  DateHandler dateHandler{dest, curr, prev};
  DisplayPanelMock displayPanel;
  TimeTravelMode mode{dateHandler};

  auto result = mode.onInput({Event::NoEvent}, displayPanel);

  EXPECT_EQ(result, Mode::InputResult::None);
}


TEST(TimeTravelModeTest, TestSwapsDates)
{
  Date dest{1955, 10, 26};
  Date curr{2021, 10, 17};
  Date prev{1985, 10, 26};
  DateHandler dateHandler{dest, curr, prev};
  DisplayPanelMock displayPanel;
  TimeTravelMode mode{dateHandler};

  EXPECT_CALL(displayPanel, updateRow1(prev));
  EXPECT_CALL(displayPanel, updateRow3(dest));

  auto result = mode.onInput({Event::ButtonPressed, Event::Button::Yellow}, displayPanel);

  EXPECT_EQ(result, Mode::InputResult::Success);
  EXPECT_EQ(dateHandler.dest(), prev);
  EXPECT_EQ(dateHandler.previous(), dest);
}


TEST(TimeTravelModeTest, TestSetsDestinationDate)
{
  Date dest{1985, 10, 26};
  Date curr{2021, 10, 17};
  Date prev{1985, 100, 26};
  Date newDate{1955, 10, 26};
  DateHandler dateHandler{dest, curr, prev};
  DisplayPanelMock displayPanel;
  TimeTravelMode mode{dateHandler};

  EXPECT_CALL(displayPanel, updateRow1(newDate));

  EXPECT_EQ(mode.onInput({Event::KeyboardPressed, 1}, displayPanel), Mode::Ok);
  EXPECT_EQ(mode.onInput({Event::KeyboardPressed, 9}, displayPanel), Mode::Ok);
  EXPECT_EQ(
      mode.onInput({Event::ButtonPressed, Event::Button::Enter}, displayPanel),
      Mode::Failure) << "Invalid date input should fail";

  EXPECT_EQ(mode.onInput({Event::KeyboardPressed, 1}, displayPanel), Mode::Ok);
  EXPECT_EQ(mode.onInput({Event::KeyboardPressed, 9}, displayPanel), Mode::Ok);
  EXPECT_EQ(mode.onInput({Event::KeyboardPressed, 5}, displayPanel), Mode::Ok);
  EXPECT_EQ(mode.onInput({Event::KeyboardPressed, 5}, displayPanel), Mode::Ok);
  EXPECT_EQ(mode.onInput({Event::ButtonPressed, Event::Button::Enter}, displayPanel), Mode::Success);

  Date expected{1955, 10, 26};
  EXPECT_EQ(dateHandler.dest(), expected);
}


TEST(TimeTravelModeTest, TestSetsCurrentDate)
{
  Date dest{1985, 10, 26};
  Date curr{2021, 10, 17};
  Date prev{1985, 100, 26};
  Date newDate{1985, 10, 17};
  DateHandler dateHandler{dest, curr, prev};
  DisplayPanelMock displayPanel;
  TimeTravelMode mode{dateHandler};

  EXPECT_CALL(displayPanel, updateRow2(newDate));

  EXPECT_EQ(mode.onInput({Event::KeyboardPressed, 1}, displayPanel), Mode::Ok);
  EXPECT_EQ(mode.onInput({Event::KeyboardPressed, 9}, displayPanel), Mode::Ok);
  EXPECT_EQ(
      mode.onInput({Event::ButtonPressed, Event::Button::Green}, displayPanel),
      Mode::Failure) << "Invalid date input should fail";

  EXPECT_EQ(mode.onInput({Event::KeyboardPressed, 1}, displayPanel), Mode::Ok);
  EXPECT_EQ(mode.onInput({Event::KeyboardPressed, 9}, displayPanel), Mode::Ok);
  EXPECT_EQ(mode.onInput({Event::KeyboardPressed, 8}, displayPanel), Mode::Ok);
  EXPECT_EQ(mode.onInput({Event::KeyboardPressed, 5}, displayPanel), Mode::Ok);
  EXPECT_EQ(mode.onInput({Event::ButtonPressed, Event::Button::Green}, displayPanel), Mode::Success);

  Date expected{1985, 10, 17};
  EXPECT_EQ(dateHandler.current(), expected);
}


TEST(TimeTravelModeTest, TestLongDateFails)
{
  Date dest{1985, 10, 26};
  Date curr{2021, 10, 17};
  Date prev{1985, 10, 26};
  DateHandler dateHandler{dest, curr, prev};
  DisplayPanelMock displayPanel;
  TimeTravelMode mode{dateHandler};

  for (uint8_t idx = 0 ; idx < 12 ; idx++) {
    EXPECT_EQ(mode.onInput({Event::KeyboardPressed, 1}, displayPanel), Mode::Ok);
  }

  EXPECT_EQ(mode.onInput({Event::KeyboardPressed, 1}, displayPanel), Mode::Failure);
}


TEST(TimeTravelModeTest, TestUpdateDateWhenNeeded)
{
  Date currentDate = Date{2021, 10, 19, 20, 49, 45};
  DateHandler dateHandler{Date{1985, 10, 26}, currentDate, Date{1985, 10, 26}};
  DisplayPanelMock displayPanel;
  TimeTravelMode mode{dateHandler};

  EXPECT_CALL(displayPanel, updateRow2(currentDate));

  EXPECT_EQ(mode.onInput({Event::TimeWentOn}, displayPanel), Mode::None);
}
