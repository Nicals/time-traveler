#include <gtest/gtest.h>

#include <ttraveler/modes/SettingsMode.h>
#include <ttraveler/Settings.h>

#include "../mocks/DisplayPanelMock.h"


using ::testing::NiceMock;
using namespace libtraveler;


TEST(SettingsModeTest, TestScrolls)
{
  Settings settings;
  DisplayPanelMock displayPanel;
  SettingsMode mode{settings};

  {
    ::testing::InSequence seq;

    // scroll up
    EXPECT_CALL(displayPanel, updateRow1(settings.soundEnabled()));
    EXPECT_CALL(displayPanel, updateRow2(settings.brightness()));
    EXPECT_CALL(displayPanel, clearRow3());

    // scroll down
    EXPECT_CALL(displayPanel, clearRow1());
    EXPECT_CALL(displayPanel, updateRow2(settings.soundEnabled()));
    EXPECT_CALL(displayPanel, updateRow3(settings.brightness()));
  }

  auto result = mode.onInput({Event::ButtonPressed, Event::Button::Red}, displayPanel);
  EXPECT_EQ(result, Mode::None);

  result = mode.onInput({Event::ButtonPressed, Event::Button::Yellow}, displayPanel);
  EXPECT_EQ(result, Mode::None);
}


TEST(SettingsModeTest, TestDisableSound)
{
  Settings settings;
  NiceMock<DisplayPanelMock> displayPanel;
  SettingsMode mode{settings};

  EXPECT_EQ(settings.soundEnabled().value(), 1);

  // toggle sound
  auto result = mode.onInput({Event::ButtonPressed, Event::Button::Green}, displayPanel);

  EXPECT_EQ(result, Mode::InputResult::Success);
  EXPECT_EQ(settings.soundEnabled().value(), 0);
}


TEST(SettingsModeTest, TestSetBrightness)
{
  Settings settings;
  NiceMock<DisplayPanelMock> displayPanel;
  SettingsMode mode{settings};

  EXPECT_NE(settings.brightness().value(), 1);

  // select second settings
  auto result = mode.onInput({Event::ButtonPressed, Event::Button::Red}, displayPanel);
  EXPECT_EQ(result, Mode::InputResult::None);

  result = mode.onInput({Event::KeyboardPressed, 1}, displayPanel);
  EXPECT_EQ(result, Mode::InputResult::None);

  result = mode.onInput({Event::ButtonPressed, Event::Button::Green}, displayPanel);
  EXPECT_EQ(result, Mode::InputResult::Success);
  EXPECT_EQ(settings.brightness().value(), 1);
}
