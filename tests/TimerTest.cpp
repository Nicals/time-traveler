#include <gtest/gtest.h>

#include "mocks/TimerMock.h"


using namespace libtraveler;


TEST(TimerTest, TestCallsObserverOnceOnTimeout)
{
  TimerObserverImpl observer;
  TimerImpl timer;
  timer.setObserver(&observer);

  timer.start();
  timer.checkTimeout();
  EXPECT_FALSE(observer.isCalled()) << "Observer called without timeout";

  timer.isTimedOut = true;
  timer.checkTimeout();
  EXPECT_TRUE(observer.isCalled()) << "Observer not called on timeout";

  observer.reset();
  timer.checkTimeout();
  EXPECT_FALSE(observer.isCalled()) << "Timer won't trigger timeout twice on same run";
}
