EESchema Schematic File Version 2
LIBS:main-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:time-traveller
LIBS:main-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCP23018 Q?
U 1 1 592043A1
P 2800 3100
F 0 "Q?" H 2850 2900 60  0000 C CNN
F 1 "MCP23018" H 2800 3100 60  0000 C CNN
F 2 "" H 3800 3750 60  0001 C CNN
F 3 "" H 3800 3750 60  0001 C CNN
	1    2800 3100
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR08
U 1 1 59204442
P 2000 2150
F 0 "#PWR08" H 2000 2000 50  0001 C CNN
F 1 "+5V" H 2000 2290 50  0000 C CNN
F 2 "" H 2000 2150 50  0000 C CNN
F 3 "" H 2000 2150 50  0000 C CNN
	1    2000 2150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 592044B1
P 2000 4150
F 0 "#PWR09" H 2000 3900 50  0001 C CNN
F 1 "GND" H 2000 4000 50  0000 C CNN
F 2 "" H 2000 4150 50  0000 C CNN
F 3 "" H 2000 4150 50  0000 C CNN
	1    2000 4150
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR010
U 1 1 592044CC
P 2000 3750
F 0 "#PWR010" H 2000 3600 50  0001 C CNN
F 1 "+5V" H 2000 3890 50  0000 C CNN
F 2 "" H 2000 3750 50  0000 C CNN
F 3 "" H 2000 3750 50  0000 C CNN
	1    2000 3750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 59204530
P 2000 2800
F 0 "#PWR011" H 2000 2550 50  0001 C CNN
F 1 "GND" H 2000 2650 50  0000 C CNN
F 2 "" H 2000 2800 50  0000 C CNN
F 3 "" H 2000 2800 50  0000 C CNN
	1    2000 2800
	1    0    0    -1  
$EndComp
Text HLabel 1250 2450 0    60   Input ~ 0
SCL
Text HLabel 1250 2550 0    60   Input ~ 0
SDA
Text HLabel 4050 3950 2    60   Output ~ 0
InputINT
$Comp
L LED D?
U 1 1 5922A63A
P 4350 1300
F 0 "D?" H 4350 1400 50  0000 C CNN
F 1 "LED" H 4350 1200 50  0000 C CNN
F 2 "" H 4350 1300 50  0000 C CNN
F 3 "" H 4350 1300 50  0000 C CNN
	1    4350 1300
	0    1    1    0   
$EndComp
$Comp
L LED D?
U 1 1 5922A9B9
P 4700 1300
F 0 "D?" H 4700 1400 50  0000 C CNN
F 1 "LED" H 4700 1200 50  0000 C CNN
F 2 "" H 4700 1300 50  0000 C CNN
F 3 "" H 4700 1300 50  0000 C CNN
	1    4700 1300
	0    1    1    0   
$EndComp
$Comp
L LED D?
U 1 1 5922A9DC
P 5050 1300
F 0 "D?" H 5050 1400 50  0000 C CNN
F 1 "LED" H 5050 1200 50  0000 C CNN
F 2 "" H 5050 1300 50  0000 C CNN
F 3 "" H 5050 1300 50  0000 C CNN
	1    5050 1300
	0    1    1    0   
$EndComp
$Comp
L LED D?
U 1 1 5922AAA5
P 5400 1300
F 0 "D?" H 5400 1400 50  0000 C CNN
F 1 "LED" H 5400 1200 50  0000 C CNN
F 2 "" H 5400 1300 50  0000 C CNN
F 3 "" H 5400 1300 50  0000 C CNN
	1    5400 1300
	0    1    1    0   
$EndComp
$Comp
L LED D?
U 1 1 5922AB31
P 5750 1300
F 0 "D?" H 5750 1400 50  0000 C CNN
F 1 "LED" H 5750 1200 50  0000 C CNN
F 2 "" H 5750 1300 50  0000 C CNN
F 3 "" H 5750 1300 50  0000 C CNN
	1    5750 1300
	0    1    1    0   
$EndComp
$Comp
L +5V #PWR012
U 1 1 5922ABA7
P 4400 1050
F 0 "#PWR012" H 4400 900 50  0001 C CNN
F 1 "+5V" H 4400 1190 50  0000 C CNN
F 2 "" H 4400 1050 50  0000 C CNN
F 3 "" H 4400 1050 50  0000 C CNN
	1    4400 1050
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5922ADFB
P 4350 1700
F 0 "R?" V 4430 1700 50  0000 C CNN
F 1 "R" V 4350 1700 50  0000 C CNN
F 2 "" V 4280 1700 50  0000 C CNN
F 3 "" H 4350 1700 50  0000 C CNN
	1    4350 1700
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5922AE7E
P 4700 1700
F 0 "R?" V 4780 1700 50  0000 C CNN
F 1 "R" V 4700 1700 50  0000 C CNN
F 2 "" V 4630 1700 50  0000 C CNN
F 3 "" H 4700 1700 50  0000 C CNN
	1    4700 1700
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5922AEA3
P 5050 1700
F 0 "R?" V 5130 1700 50  0000 C CNN
F 1 "R" V 5050 1700 50  0000 C CNN
F 2 "" V 4980 1700 50  0000 C CNN
F 3 "" H 5050 1700 50  0000 C CNN
	1    5050 1700
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5922AECE
P 5400 1700
F 0 "R?" V 5480 1700 50  0000 C CNN
F 1 "R" V 5400 1700 50  0000 C CNN
F 2 "" V 5330 1700 50  0000 C CNN
F 3 "" H 5400 1700 50  0000 C CNN
	1    5400 1700
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5922AF05
P 5750 1700
F 0 "R?" V 5830 1700 50  0000 C CNN
F 1 "R" V 5750 1700 50  0000 C CNN
F 2 "" V 5680 1700 50  0000 C CNN
F 3 "" H 5750 1700 50  0000 C CNN
	1    5750 1700
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X11 P?
U 1 1 59299CD2
P 9400 3300
F 0 "P?" H 9400 3900 50  0000 C CNN
F 1 "Keypad Connection" V 9500 3300 50  0000 C CNN
F 2 "" H 9400 3300 50  0000 C CNN
F 3 "" H 9400 3300 50  0000 C CNN
	1    9400 3300
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 5929B022
P 9000 1150
F 0 "R?" V 9080 1150 50  0000 C CNN
F 1 "R" V 9000 1150 50  0000 C CNN
F 2 "" V 8930 1150 50  0000 C CNN
F 3 "" H 9000 1150 50  0000 C CNN
	1    9000 1150
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5929B0C7
P 9200 1150
F 0 "R?" V 9280 1150 50  0000 C CNN
F 1 "R" V 9200 1150 50  0000 C CNN
F 2 "" V 9130 1150 50  0000 C CNN
F 3 "" H 9200 1150 50  0000 C CNN
	1    9200 1150
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5929B0FA
P 9400 1150
F 0 "R?" V 9480 1150 50  0000 C CNN
F 1 "R" V 9400 1150 50  0000 C CNN
F 2 "" V 9330 1150 50  0000 C CNN
F 3 "" H 9400 1150 50  0000 C CNN
	1    9400 1150
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5929B12F
P 9600 1150
F 0 "R?" V 9680 1150 50  0000 C CNN
F 1 "R" V 9600 1150 50  0000 C CNN
F 2 "" V 9530 1150 50  0000 C CNN
F 3 "" H 9600 1150 50  0000 C CNN
	1    9600 1150
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5929B166
P 9800 1150
F 0 "R?" V 9880 1150 50  0000 C CNN
F 1 "R" V 9800 1150 50  0000 C CNN
F 2 "" V 9730 1150 50  0000 C CNN
F 3 "" H 9800 1150 50  0000 C CNN
	1    9800 1150
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5929B19F
P 10000 1150
F 0 "R?" V 10080 1150 50  0000 C CNN
F 1 "R" V 10000 1150 50  0000 C CNN
F 2 "" V 9930 1150 50  0000 C CNN
F 3 "" H 10000 1150 50  0000 C CNN
	1    10000 1150
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5929B1DC
P 10200 1150
F 0 "R?" V 10280 1150 50  0000 C CNN
F 1 "R" V 10200 1150 50  0000 C CNN
F 2 "" V 10130 1150 50  0000 C CNN
F 3 "" H 10200 1150 50  0000 C CNN
	1    10200 1150
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5929B21B
P 10400 1150
F 0 "R?" V 10480 1150 50  0000 C CNN
F 1 "R" V 10400 1150 50  0000 C CNN
F 2 "" V 10330 1150 50  0000 C CNN
F 3 "" H 10400 1150 50  0000 C CNN
	1    10400 1150
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5929B25C
P 10600 1150
F 0 "R?" V 10680 1150 50  0000 C CNN
F 1 "R" V 10600 1150 50  0000 C CNN
F 2 "" V 10530 1150 50  0000 C CNN
F 3 "" H 10600 1150 50  0000 C CNN
	1    10600 1150
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5929B2A3
P 10800 1150
F 0 "R?" V 10880 1150 50  0000 C CNN
F 1 "R" V 10800 1150 50  0000 C CNN
F 2 "" V 10730 1150 50  0000 C CNN
F 3 "" H 10800 1150 50  0000 C CNN
	1    10800 1150
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR013
U 1 1 5929DEEE
P 9050 900
F 0 "#PWR013" H 9050 750 50  0001 C CNN
F 1 "+5V" H 9050 1040 50  0000 C CNN
F 2 "" H 9050 900 50  0000 C CNN
F 3 "" H 9050 900 50  0000 C CNN
	1    9050 900 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 5929EA26
P 8700 3300
F 0 "#PWR014" H 8700 3050 50  0001 C CNN
F 1 "GND" H 8700 3150 50  0000 C CNN
F 2 "" H 8700 3300 50  0000 C CNN
F 3 "" H 8700 3300 50  0000 C CNN
	1    8700 3300
	1    0    0    -1  
$EndComp
$Comp
L CD40147 Q?
U 1 1 5929FE79
P 8150 2150
F 0 "Q?" H 8150 1900 60  0000 C CNN
F 1 "CD40147" H 8150 2150 60  0000 C CNN
F 2 "" H 8150 2150 60  0001 C CNN
F 3 "" H 8150 2150 60  0001 C CNN
	1    8150 2150
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR015
U 1 1 592A0655
P 7450 1800
F 0 "#PWR015" H 7450 1550 50  0001 C CNN
F 1 "GND" H 7450 1650 50  0000 C CNN
F 2 "" H 7450 1800 50  0000 C CNN
F 3 "" H 7450 1800 50  0000 C CNN
	1    7450 1800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR016
U 1 1 592A0730
P 7450 1450
F 0 "#PWR016" H 7450 1300 50  0001 C CNN
F 1 "+5V" H 7450 1590 50  0000 C CNN
F 2 "" H 7450 1450 50  0000 C CNN
F 3 "" H 7450 1450 50  0000 C CNN
	1    7450 1450
	1    0    0    -1  
$EndComp
Text Label 7400 2150 2    60   ~ 0
Keypad_D
Text Label 7400 2250 2    60   ~ 0
Keypad_C
Text Label 7400 2350 2    60   ~ 0
Keypad_B
Text Label 7400 2450 2    60   ~ 0
Keypad_A
Text Label 3750 3550 0    60   ~ 0
Keypad_A
Text Label 3750 3650 0    60   ~ 0
Keypad_B
Text Label 3750 3750 0    60   ~ 0
Keypad_C
Text Label 3750 2750 0    60   ~ 0
Keypad_D
Text Notes 8900 3550 0    60   ~ 0
Connections follows keypad order
Wire Wire Line
	2000 2150 2000 2250
Wire Wire Line
	2000 2250 2100 2250
Wire Wire Line
	2100 4050 2000 4050
Wire Wire Line
	2000 4050 2000 4150
Wire Wire Line
	2000 3750 2000 3850
Wire Wire Line
	2000 3850 2100 3850
Wire Wire Line
	2000 2800 2000 2700
Wire Wire Line
	2000 2700 2100 2700
Wire Wire Line
	1250 2450 2100 2450
Wire Wire Line
	1250 2550 2100 2550
Wire Wire Line
	4050 3950 3550 3950
Wire Wire Line
	4350 1150 4350 1050
Wire Wire Line
	4350 1050 5750 1050
Wire Wire Line
	5750 1050 5750 1150
Wire Wire Line
	5750 1450 5750 1550
Wire Wire Line
	5400 1450 5400 1550
Wire Wire Line
	5050 1450 5050 1550
Wire Wire Line
	4700 1450 4700 1550
Wire Wire Line
	4350 1450 4350 1550
Wire Wire Line
	4350 1850 4350 2250
Wire Wire Line
	4350 2250 3550 2250
Wire Wire Line
	3550 2350 4700 2350
Wire Wire Line
	4700 2350 4700 1850
Wire Wire Line
	5050 1850 5050 2450
Wire Wire Line
	5050 2450 3550 2450
Wire Wire Line
	3550 2550 5400 2550
Wire Wire Line
	5400 2550 5400 1850
Wire Wire Line
	5750 1850 5750 2650
Wire Wire Line
	5750 2650 3550 2650
Wire Wire Line
	4700 1150 4700 1050
Connection ~ 4700 1050
Wire Wire Line
	5050 1150 5050 1050
Connection ~ 5050 1050
Wire Wire Line
	5400 1150 5400 1050
Connection ~ 5400 1050
Wire Wire Line
	9000 1300 9000 3100
Wire Wire Line
	9200 1300 9200 2500
Wire Wire Line
	9600 1300 9600 2600
Wire Wire Line
	9800 1300 9800 2650
Wire Wire Line
	10000 1300 10000 2700
Wire Wire Line
	10200 1300 10200 2750
Wire Wire Line
	10400 1300 10400 2800
Wire Wire Line
	10600 1300 10600 2850
Wire Wire Line
	10800 1300 10800 2900
Wire Wire Line
	9200 2500 9100 2500
Wire Wire Line
	9100 2500 9100 3100
Wire Wire Line
	9400 2550 9200 2550
Wire Wire Line
	9200 2550 9200 3100
Wire Wire Line
	9600 2600 9300 2600
Wire Wire Line
	9300 2600 9300 3100
Wire Wire Line
	9800 2650 9400 2650
Wire Wire Line
	9400 2650 9400 3100
Wire Wire Line
	10000 2700 9500 2700
Wire Wire Line
	9500 2700 9500 3100
Wire Wire Line
	9600 2750 9600 3100
Wire Wire Line
	9700 2800 9700 3100
Wire Wire Line
	9800 2850 9800 3100
Wire Wire Line
	10200 2750 9600 2750
Wire Wire Line
	10400 2800 9700 2800
Wire Wire Line
	10600 2850 9800 2850
Wire Wire Line
	9900 3100 9900 2900
Wire Wire Line
	9900 2900 10800 2900
Wire Wire Line
	9000 1000 9000 900 
Wire Wire Line
	9000 900  10800 900 
Wire Wire Line
	10800 900  10800 1000
Wire Wire Line
	10600 1000 10600 900 
Connection ~ 10600 900 
Wire Wire Line
	10400 1000 10400 900 
Connection ~ 10400 900 
Wire Wire Line
	10200 1000 10200 900 
Connection ~ 10200 900 
Wire Wire Line
	10000 1000 10000 900 
Connection ~ 10000 900 
Wire Wire Line
	9800 1000 9800 900 
Connection ~ 9800 900 
Wire Wire Line
	9600 1000 9600 900 
Connection ~ 9600 900 
Wire Wire Line
	9400 1000 9400 900 
Connection ~ 9400 900 
Wire Wire Line
	9200 1000 9200 900 
Connection ~ 9200 900 
Wire Wire Line
	8700 3300 8700 3050
Wire Wire Line
	8700 3050 8900 3050
Wire Wire Line
	8900 3050 8900 3100
Wire Wire Line
	7450 1800 7450 1650
Wire Wire Line
	7450 1650 7550 1650
Wire Wire Line
	7450 1450 7450 1550
Wire Wire Line
	7450 1550 7550 1550
Wire Wire Line
	7550 2150 7400 2150
Wire Wire Line
	7550 2250 7400 2250
Wire Wire Line
	7550 2350 7400 2350
Wire Wire Line
	7550 2450 7400 2450
Wire Wire Line
	3750 3550 3550 3550
Wire Wire Line
	3550 3650 3750 3650
Wire Wire Line
	3750 3750 3550 3750
Wire Wire Line
	3750 2750 3550 2750
Wire Wire Line
	9400 1300 9400 2550
Wire Wire Line
	8750 1750 9000 1750
Connection ~ 9000 1750
Wire Wire Line
	8750 2050 9200 2050
Connection ~ 9200 2050
Wire Wire Line
	8750 2350 9400 2350
Connection ~ 9400 2350
Wire Wire Line
	8750 2450 9600 2450
Connection ~ 9600 2450
Wire Wire Line
	8750 1650 9800 1650
Connection ~ 9800 1650
Wire Wire Line
	8750 1950 10000 1950
Connection ~ 10000 1950
Wire Wire Line
	8750 2250 10200 2250
Connection ~ 10200 2250
Wire Wire Line
	8750 1550 10400 1550
Connection ~ 10400 1550
Wire Wire Line
	8750 1850 10600 1850
Connection ~ 10600 1850
Wire Wire Line
	8750 2150 10800 2150
Connection ~ 10800 2150
$EndSCHEMATC
